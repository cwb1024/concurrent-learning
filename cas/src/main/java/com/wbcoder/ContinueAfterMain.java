package com.wbcoder;

public class ContinueAfterMain {

    public static void main(String[] args) {


        int i = 2;

        for (int j = 0; j < 13; j++) {
            if (j == i) {
                continue;
            }
            System.out.println(j);
        }
    }
}
