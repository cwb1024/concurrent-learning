package com.wbcoder.cas;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

public class CasThreadClient {

    private static AtomicInteger count = new AtomicInteger(0);

    //利用CAS进行操作，保证多个线程多共享变量的操作，不会出现线程安全问题

    public void add1() throws Exception {
        //这里模拟多个线程，进行并发的加操作
        CountDownLatch countDownLatch = new CountDownLatch(50);
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                try {
                    Thread.sleep(100);
                    count.getAndIncrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                countDownLatch.countDown();
                System.out.println(String.format("ThreadName=%s,已经操作完成，并且闭锁进行了减1操作."
                ,Thread.currentThread().getName()));
            }).start();
        }
        countDownLatch.await();
        System.out.println("main is await...");
        System.out.println(count);
    }

    public void add2() throws Exception {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(50);
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                count.getAndIncrement();
                try {
                    cyclicBarrier.await();
                    System.out.println(String.format("ThreadName=%s正在等待其他线程.", Thread.currentThread().getName()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        System.out.println("main is out print...");
        System.out.println(count);
    }


    public int[] twoSum (int[] numbers, int target) {
        // write code here
        Map<Integer,Integer> complementMap = new HashMap<>();
        for(int i=0;i<numbers.length;i++)
        {
            Integer complementIndex = complementMap.get(numbers[i]);
            if(complementIndex != null){
                if(complementIndex < i)
                {
                    return new int[]{complementIndex+1,i+1};
                }else
                {
                    return new int[]{i+1,complementIndex+1};
                }
            }
            complementMap.put(target-numbers[i],i);
        }
        return new int[]{-1,-1};
    }

    public static void main(String[] args) throws Exception {
        CasThreadClient casThreadClient = new CasThreadClient();
        casThreadClient.add1();
//        casThreadClient.add2();
    }

}
