package com.wbcoder.delay.queue;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DelayTask implements Runnable, Delayed {

    private String taskName;
    private String setRunTime;
    private String taskId;

    public DelayTask(String taskName, String setRunTime, String taskId) {
        this.taskName = taskName;
        this.setRunTime = setRunTime;
        this.taskId = taskId;
    }

    public int compareTo(Delayed o) {
        return 0;
    }

    public void run() {

    }

    public long getDelay(TimeUnit unit) {
        return 0;
    }
}
