package com.wbcoder.collections.list;

import java.util.concurrent.CopyOnWriteArrayList;

/**
  * @description: 这里怎么模拟抢一个锁的情况
  * @author: chengwb
  * @Date: 2020-01-05 04:50
  */
public class AddRunnable implements Runnable {

    private CopyOnWriteArrayList copyOnWriteArrayList = null;

    private Object data;

    public AddRunnable(CopyOnWriteArrayList copyOnWriteArrayList, Object data) {
        this.copyOnWriteArrayList = copyOnWriteArrayList;
        this.data = data;
    }

    @Override
    public void run() {

        System.out.println("开始执行加数据。。。。");

        copyOnWriteArrayList.add(data);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("加数据执行完成。。。。");

    }
}
