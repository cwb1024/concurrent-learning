package com.wbcoder.collections.list;

import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteArrayListClient {

    public static void main(String[] args) throws InterruptedException {

        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();

        Integer i = 1;

        Thread thread1 = new Thread(new AddRunnable(copyOnWriteArrayList, i));

        Thread thread2 = new Thread(new RemoveRunnable(copyOnWriteArrayList, i));

        thread1.start();

        thread2.start();

        thread1.join();

        thread2.join();

        System.out.println(copyOnWriteArrayList);

    }
}
