package com.wbcoder.collections.list;

import java.util.concurrent.CopyOnWriteArrayList;

public class RemoveRunnable implements Runnable {

    private CopyOnWriteArrayList copyOnWriteArrayList = null;

    private Object data;

    public RemoveRunnable(CopyOnWriteArrayList copyOnWriteArrayList, Object data) {
        this.copyOnWriteArrayList = copyOnWriteArrayList;
        this.data = data;
    }

    @Override
    public void run() {

        System.out.println("开始删除数据。。。");

        copyOnWriteArrayList.remove(data);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("执行删除数据完成。。。");
    }
}
