package com.wbcoder.lock.custom;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;

/**
 * @description: 实现一个生产者消费者模型，利用刚才实现的非重入锁
 * @author: chengwb
 * @Date: 2020-04-12 01:30
 */
public class PCClient {

    final static NonReenTrantLock lock = new NonReenTrantLock();

    final static Condition notFull = lock.newCondition();

    final static Condition notEmpty = lock.newCondition();

    final static Queue<String> queue = new LinkedBlockingQueue<>();

    final static int queueSize = 10;

    public static void main(String[] args) {

        //共享一个进程内的资源，这里需要开启多个子线程，来实现这个生产者、消费者模型

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    //获取独占锁
                    System.out.println("入队列线程开始抢锁，" + Thread.currentThread().getId());
                    lock.lock();
                    try {

                        //如果队列满了，就不往队列里添加了

                        while (queue.size() == queueSize) {
                            System.out.println("队列满了，开始等待。。。。。");
                            notEmpty.await();
                            System.out.println("队列满了，受到可以继续生产的信号，继续执行。。。。。");
                        }


                        //添加元素
                        boolean add = queue.add("ele" + i);
                        if (add) {
                            System.out.println("入队================> ele" + i);
                        }

                        Thread.sleep(500);

                        //有元素了，可以通知消费者线程进行消费了
                        notFull.signalAll();
                        System.out.println("入队完 信号通知也执行了");

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        lock.unlock();
                        System.out.println("入队完 这个线程释放了锁："+Thread.currentThread().getId());
                    }
                }
            }
        });

        Thread consumer = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    //获取独占锁
                    System.out.println("出队列线程开始抢锁，" + Thread.currentThread().getId());
                    lock.lock();
                    try {
                        //如果对空，就等待者这个队不空
                        if (queue.isEmpty()) {
                            System.out.println("队列空了，开始等待。。。。。");
                            notFull.await();
                            System.out.println("队列空了，受到可以继续消费的信号，继续执行。。。。。");
                        }

                        //消费一个元素
                        Thread.sleep(700);
                        String ele = queue.poll();
                        System.out.println(ele);

                        //唤醒生产线程
                        notEmpty.signalAll();


                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        lock.unlock();
                    }
                }
            }
        });

        //启动线程
        producer.start();
        consumer.start();


    }
}
