package com.wbcoder.lock.induction;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @description: 简单使用一下 lock 的 API
 * @author: chengwb
 * @Date: 2019-09-03 01:09
 */
public class Api {

    public static void main(String[] args) {
        Lock lock = new ReentrantLock();

        //加锁
        lock.lock();

        //解锁
        lock.unlock();

        //试图尝试获取锁
        lock.tryLock();

        //尝试在 1000s 内 获取锁
        try {
            lock.tryLock(1000, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //
        Condition condition = lock.newCondition();

        //
        try {
            lock.lockInterruptibly();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ReentrantLock reentrantLock = new ReentrantLock();

        reentrantLock.isLocked();

        reentrantLock.isFair();

        reentrantLock.getHoldCount();

        reentrantLock.getQueueLength();

        reentrantLock.getWaitQueueLength(condition);

        reentrantLock.hasQueuedThreads();

        reentrantLock.hasWaiters(condition);

        reentrantLock.isHeldByCurrentThread();


        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        ReentrantReadWriteLock.ReadLock readLock = new ReentrantReadWriteLock().readLock();

        //readLock 也即是 lock 的基本方法

        ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();

        ReentrantReadWriteLock.ReadLock readLock1 = readWriteLock.readLock();


    }

}
