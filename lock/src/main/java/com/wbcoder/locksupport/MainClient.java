package com.wbcoder.locksupport;

/**
  * @description: 找几个自线程验证下  LockSupport
  * @author: chengwb
  * @Date: 2020-04-12 00:20
  */
public class MainClient {

    public static void main(String[] args) {

        Thread tA = new Thread(new ThreadA());

        tA.start();

        new Thread(new ThreadB(tA)).start();

    }
}
