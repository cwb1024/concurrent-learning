package com.wbcoder.locksupport;

import java.util.concurrent.locks.LockSupport;

public class ThreadA implements Runnable {

    @Override
    public void run() {
        System.out.println("begin ThreadA running...");

        System.out.println("begin use LockSupport park");

        LockSupport.park();

        System.out.println(".......pack 被打断了");

        System.out.println("begin ThreadA running  again...");

        int i = 100;
        while (i > 0) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(i--);

        }

    }
}
