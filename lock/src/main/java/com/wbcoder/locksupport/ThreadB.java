package com.wbcoder.locksupport;

import java.util.concurrent.locks.LockSupport;

public class ThreadB implements Runnable {

    private Thread thread;

    public ThreadB(Thread thread) {
        this.thread = thread;
    }

    @Override
    public void run() {

        System.out.println("ThreadB is running...");

        int i = 10;
        while (i > 0) {
            try {
                Thread.sleep(200);
                System.out.println("ThreadB is sleeping 0.2s * 10");
                i--;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("ThreadB 调用 unpack....");

        LockSupport.unpark(thread);

    }
}
