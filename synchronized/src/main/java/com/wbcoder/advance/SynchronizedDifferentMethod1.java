package com.wbcoder.advance;

/**
 * @description: 多个线程同时访问一个类对象实例的 不同的同步方法， 串行
 * @author: chengwb
 * @Date: 2019-08-28 02:13
 */
public class SynchronizedDifferentMethod1 implements Runnable {

    @Override
    public void run() {
        if (Thread.currentThread().getName().equals("Thread-0")) {
            synchronizeMethod1();
        } else {
            synchronizeMethod2();
        }
    }

    /**
     * 同步方法1
     */
    public synchronized void synchronizeMethod1() {
        System.out.println("同步方法1 == 》 开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("同步方法1 == 》 结束执行，我是" + Thread.currentThread().getName());
    }

    /**
     * 同步方法2
     */
    public synchronized void synchronizeMethod2() {
        System.out.println("同步方法2 == 》 开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("同步方法2 == 》 结束执行，我是" + Thread.currentThread().getName());
    }

    static SynchronizedDifferentMethod1 synchronizedYesAndNo11 = new SynchronizedDifferentMethod1();
    static SynchronizedDifferentMethod1 synchronizedYesAndNo12 = new SynchronizedDifferentMethod1();

    public static void main(String[] args) {
        Thread t1 = new Thread(synchronizedYesAndNo11);
        Thread t2 = new Thread(synchronizedYesAndNo11);

        t1.start();
        t2.start();

        while (t1.isAlive() || t2.isAlive()) {

        }

        System.out.println("程序结束。");
    }
}
