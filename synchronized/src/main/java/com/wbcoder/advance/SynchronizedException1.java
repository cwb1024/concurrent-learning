package com.wbcoder.advance;

/**
  * @description: 方法抛出异常后，会释放锁。
 *  模拟场景展示不抛出异常前和抛出异常后的对比：一旦一个线程抛出异常，第二个线程会立刻进入同步方法，说明会释放锁了
  * @author: chengwb
  * @Date: 2019-08-28 02:41
  */
public class SynchronizedException1 implements Runnable{

    @Override
    public void run() {
        if (Thread.currentThread().getName().equals("Thread-0")) {
            synchronizeMethod();
        } else {
            normalMethod();
        }
    }

    /**
     *
     */
    public synchronized void synchronizeMethod() {
        System.out.println("同步方法 ，我是加锁方法== 》 开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new RuntimeException();
        //System.out.println("同步方法 ，我是加锁方法== 》 结束执行，我是" + Thread.currentThread().getName());
    }

    /**
     * 普通同步方法 对象锁
     */
    public synchronized void normalMethod() {
        System.out.println("同步方法 ，我是加锁方法== 》 开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("同步方法，我是加锁方法 == 》 结束执行，我是" + Thread.currentThread().getName());
    }

    static SynchronizedException1 synchronizedYesAndNo11 = new SynchronizedException1();

    public static void main(String[] args) {
        Thread t1 = new Thread(synchronizedYesAndNo11);
        Thread t2 = new Thread(synchronizedYesAndNo11);

        t1.start();
        t2.start();

        while (t1.isAlive() || t2.isAlive()) {

        }

        System.out.println("程序结束。");
    }
}
