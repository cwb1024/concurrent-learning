package com.wbcoder.advance;

/**
 * @description: 同时访问静态同步方法，非静态同步方法
 * @author: chengwb
 * @Date: 2019-08-28 02:13
 */
public class SynchronizedStaticAndNormal1 implements Runnable {

    @Override
    public void run() {
        if (Thread.currentThread().getName().equals("Thread-0")) {
            synchronizeMethod();
        } else {
            normalMethod();
        }
    }

    /**
     * 静态同步方法 Class锁
     */
    public synchronized static void synchronizeMethod() {
        System.out.println("静态同步方法 ，我是静态加锁方法== 》 开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("静态同步方法 ，我是静态加锁方法== 》 结束执行，我是" + Thread.currentThread().getName());
    }

    /**
     * 普通同步方法 对象锁
     */
    public synchronized void normalMethod() {
        System.out.println("普通同步方法 ，我是普通加锁方法== 》 开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("普通同步方法，我是普通加锁方法 == 》 结束执行，我是" + Thread.currentThread().getName());
    }

    static SynchronizedStaticAndNormal1 synchronizedYesAndNo11 = new SynchronizedStaticAndNormal1();

    public static void main(String[] args) {
        Thread t1 = new Thread(synchronizedYesAndNo11);
        Thread t2 = new Thread(synchronizedYesAndNo11);

        t1.start();
        t2.start();

        while (t1.isAlive() || t2.isAlive()) {

        }

        System.out.println("程序结束。");
    }
}
