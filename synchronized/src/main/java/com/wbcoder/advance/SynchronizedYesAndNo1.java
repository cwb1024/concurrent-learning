package com.wbcoder.advance;

/**
 * @description: 同时访问同步方法，非同步方法
 * @author: chengwb
 * @Date: 2019-08-28 02:13
 */
public class SynchronizedYesAndNo1 implements Runnable {

    @Override
    public void run() {
        if (Thread.currentThread().getName().equals("Thread-0")) {
            synchronizeMethod();
        } else {
            normalMethod();
        }
    }

    /**
     * 同步方法
     */
    public synchronized void synchronizeMethod() {
        System.out.println("同步方法 == 》 开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("同步方法 == 》 结束执行，我是" + Thread.currentThread().getName());
    }

    /**
     * 普通方法
     */
    public void normalMethod() {
        System.out.println("普通方法 == 》 开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("普通方法 == 》 结束执行，我是" + Thread.currentThread().getName());
    }

    static SynchronizedYesAndNo1 synchronizedYesAndNo11 = new SynchronizedYesAndNo1();

    public static void main(String[] args) {
        Thread t1 = new Thread(synchronizedYesAndNo11);
        Thread t2 = new Thread(synchronizedYesAndNo11);

        t1.start();
        t2.start();

        while (t1.isAlive() || t2.isAlive()) {

        }

        System.out.println("程序结束。");
    }
}
