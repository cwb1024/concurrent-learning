package com.wbcoder.advance.principle;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
  * @description: 加锁释放锁的等价代码
  * @author: chengwb
  * @Date: 2019-08-31 01:19
  */
public class SynchronizeToLock {

    Lock lock = new ReentrantLock();

    public synchronized void method1(){
        System.out.println("我是Synchronized形式的同步方法method1");
    }

    /**
      * @description: 模拟synchronize 进入同步方法加锁，退出同步方法释放锁
      * @author: chengwb
      * @Date: 2019-08-31 01:23
      */
    public void method2(){
        lock.lock();
        try {
            System.out.println("我是Lock形式的同步方法method1");
        }finally {
            //一定会执行到，就能保证退出代码块一定会释放锁
            lock.unlock();
            //todo 如果注释掉，代码执行完毕，也会释放掉锁？
        }
    }


    public static void main(String[] args) {
        SynchronizeToLock synchronizeToLock = new SynchronizeToLock();
        synchronizeToLock.method2();
        synchronizeToLock.method1();
    }

}
