package com.wbcoder.advance.principle.decompilation;

/**
  * @description: 反编译一个字节码，看到一些指令
 *                进入当前目录 运行 JavaC 命令 获取到字节码文件
 *                利用 javap -verbose xxx.class 查看指令
  * @author: chengwb
  * @Date: 2019-08-31 01:46
  */
public class Decompilation1 {

    private Object object = new Object();

    public void method1(){
        synchronized (object) {

        }
    }

}
