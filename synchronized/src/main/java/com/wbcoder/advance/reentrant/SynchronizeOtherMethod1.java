package com.wbcoder.advance.reentrant;

/**
  * @description: 可重入粒度测试，调用类内的同步方法
  * @author: chengwb
  * @Date: 2019-08-31 00:40
  */
public class SynchronizeOtherMethod1 {

    private synchronized void method1(){
        System.out.println("执行method1");
        method2();
    }


    private synchronized void method2(){
        System.out.println("执行method2");
    }

    public static void main(String[] args) {
        SynchronizeOtherMethod1 synchronizeOtherMethod1 = new SynchronizeOtherMethod1();
        synchronizeOtherMethod1.method1();
    }
}
