package com.wbcoder.advance.reentrant;

/**
  * @description: 可重入粒度测试 ： 递归调用本方法,获取到一次锁之后，后面可以直接获取
  * @author: chengwb
  * @Date: 2019-08-30 00:59
  */
public class SynchronizeRecursion1 {

    int a = 0;

    public static void main(String[] args) {


        SynchronizeRecursion1 synchronizeRecursion1 = new SynchronizeRecursion1();


        synchronizeRecursion1.method1();
    }

    private synchronized void method1() {

        System.out.println("执行了同步方法method1,a = "+a);
        if (a == 0) {
            a++;
            method1();
        }

    }
}
