package com.wbcoder.advance.reentrant;

/**
 * @description: 可重入粒度测试，调用父类方法
 * @author: chengwb
 * @Date: 2019-08-31 00:51
 */
public class SynchronizeSuperClass {

    public synchronized void doSomething() {
        System.out.println("我是父类");
    }
}

class SubClass extends SynchronizeSuperClass {
    @Override
    public synchronized void doSomething() {
        System.out.println("我是子类");
        super.doSomething();
    }


    public static void main(String[] args) {
        SubClass subClass = new SubClass();
        subClass.doSomething();
    }
}

/**
 *
 * out:
 *
 *  我是子类
 *  我是父类
 *
 */



