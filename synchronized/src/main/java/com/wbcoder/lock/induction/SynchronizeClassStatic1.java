package com.wbcoder.lock.induction;

/**
  * @description: 类锁的第一种实现方式，static形式
  * @author: chengwb
  * @Date: 2019-08-28 01:21
  */
public class SynchronizeClassStatic1 implements Runnable{

    //申明为静态，为了 main 调用
    static SynchronizeClassStatic1 instance1 = new SynchronizeClassStatic1();

    static SynchronizeClassStatic1 instance2 = new SynchronizeClassStatic1();

    //静态方法类锁形式,加了一个static 该类对象的多个实例，会同时竞争 类锁串行化。
    public static synchronized void staticClassLockMethod(){

        System.out.println("我是类锁的第一种形式，把 synchronized 关键字加在静态方法上 == 》开始执行，我是" + Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("我是类锁的第一种形式，把 synchronized 关键字加在静态方法上 == 》结束执行，我是" + Thread.currentThread().getName());
    }


    @Override
    public void run() {
        staticClassLockMethod();
    }

    public static void main(String[] args) {

        Thread t1 = new Thread(instance1);

        Thread t2 = new Thread(instance2);

        t1.start();

        t2.start();

        while (t1.isAlive() || t2.isAlive()) {

        }

        System.out.println("程序结束。");
    }
}
