package com.wbcoder.lock.induction;


/**
  * @description: 对象锁，同一个实例对象，锁住同步代码块使用 this对象锁，单实例内顺序执行
  * @author: chengwb
  * @Date: 2019-08-28 00:27
  */
public class SynchronizeObjectCodeBlock1 implements Runnable{

    static SynchronizeObjectCodeBlock1 instance1 = new SynchronizeObjectCodeBlock1();
//    static SynchronizeObjectCodeBlock1 instance2 = new SynchronizeObjectCodeBlock1();

    @Override
    public void run() {
        //同步代码块，this 锁   当前类对象 ，只有一个同步代码块，不存在多个，存在多个有锁只有一个，或者可以多个执行的时候，搞一个对象锁，而不是当前类对象
        synchronized (this) {
            System.out.println("我是对象锁，同步代码块形式，用this当前类的实例对象 == 》 开始执行，我是 " + Thread.currentThread().getName());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("我是对象锁，同步代码块形式，用this当前类的实例对象 == 》 结束执行，我是 " + Thread.currentThread().getName());
        }
    }


    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(instance1);

        Thread t2 = new Thread(instance1);

        t1.start();

        t2.start();

        while (t1.isAlive() || t2.isAlive()) {


        }
        System.out.println("程序结束。");
    }
}
