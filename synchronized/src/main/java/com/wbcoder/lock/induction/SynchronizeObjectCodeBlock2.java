package com.wbcoder.lock.induction;


/**
  * @description: 对象锁，同一个实例对象，逻辑内使用多个锁，不能使用当前类实例对象
  * @author: chengwb
  * @Date: 2019-08-28 00:27
  */
public class SynchronizeObjectCodeBlock2 implements Runnable{

    static SynchronizeObjectCodeBlock2 instance = new SynchronizeObjectCodeBlock2();

    Object lock1 = new Object();

    Object lock2 = new Object();

    @Override
    public void run() {

        //同步代码块，对象锁，多个锁，锁住了多种资源，没有被占用的，会执行，就不会串行
        synchronized (lock1) {
            try {
                System.out.println("我是lock1，我是 " + Thread.currentThread().getName());
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "执行结束。");
        }

        synchronized (lock2) {
            try {
                System.out.println("我是lock2，我是 " + Thread.currentThread().getName());
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "执行结束。");
        }
    }


    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(instance);

        Thread t2 = new Thread(instance);

        t1.start();

        t2.start();

        while (t1.isAlive() || t2.isAlive()) {


        }
        System.out.println("程序结束。");
    }
}
