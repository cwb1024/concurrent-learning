package com.wbcoder.lock.induction;


/**
  * @description: 对象锁，同一个实例对象，synchronize 加在普通方法上，当前实例对象执行的时候串行，
 *                开启多个实例 - > 并行，单例 - >串行
  * @author: chengwb
  * @Date: 2019-08-28 00:27
  */
public class SynchronizeObjectMethod1 implements Runnable{

    static SynchronizeObjectMethod1 instance1 = new SynchronizeObjectMethod1();

//    static SynchronizeObjectMethod1 instance2 = new SynchronizeObjectMethod1();

    @Override
    public void run() {
        method();
    }

    public synchronized void method(){
        System.out.println("我是对象锁，synchronized修饰普通方法方法，当前对象成为同步方法 == 》" +
                "开始执行，我叫"+Thread.currentThread().getName());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("我是对象锁，synchronized修饰普通方法方法，当前对象成为同步方法 == 》" +
                "结束执行，我叫"+Thread.currentThread().getName());
    }


    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(instance1);

        Thread t2 = new Thread(instance1);

        t1.start();

        t2.start();

        while (t1.isAlive() || t2.isAlive()) {


        }
        System.out.println("程序结束。");
    }
}
