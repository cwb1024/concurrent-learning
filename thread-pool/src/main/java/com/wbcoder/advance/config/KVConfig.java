package com.wbcoder.advance.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 配置中心配置的KV
 */
@Configuration
public class KVConfig {


    @Value("${app.id}")
    public String appId;

    /**
     * 应用环境
     */
    @Value("${app.env}")
    public String appEnv;

    /**
     * 钉钉消息地址
     */
    @Value("${dingdingUrl}")
    public String dingdingUrl;
    /**
     * 动态线程池健康检查时间间隔
     */
    @Value("${dynamic_thread_pool.health_check.interval:1000}")
    public long dynamicThreadPoolHealthCheckInterval;

    /**
     * 报警是否显示所有堆栈信息
     */
    @Value("${switch.notify.alltrace.show}")
    public boolean switchNotifyAllTraceShow;

    @Value("${common_send_env_list:local,dev,uat,pre,pro}")
    public String commonSendEnvList;

    @Value("${dingDingSecretKey}")
    public String dingDingSecretKey;
}




