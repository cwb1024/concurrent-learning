package com.wbcoder.advance.enums;

import lombok.Getter;

/**
 * 业务报警类型
 */
@Getter
public enum BizNotifyTypeEnum {

    COMMON("common", "通用"),
    DING_DING("dd", "钉钉");

    public static BizNotifyTypeEnum getByCode(String code) {
        if (code == null || "".equals(code)) {
            return null;
        }
        BizNotifyTypeEnum[] values = BizNotifyTypeEnum.values();
        for (BizNotifyTypeEnum e : values) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

    private String code;
    private String desc;

    BizNotifyTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
