package com.wbcoder.advance.enums;

import lombok.Getter;

/**
 * 响应code枚举类
 */
@Getter
public enum ResCodeEnum {

    CODE_SUCCESS(200, "处理成功"),
    CODE_TIMEOUT_AUTHORIZATION(1001, "登陆授权超时"),
    CODE_TIMEOUT_FEIGN_CALL(1002, "Feign调用超时"),
    CODE_FEIGN_FALLBACK(1003,"Feign调用Fallback"),
    CODE_AUTH_EXCLUDE(1004,"用户授权互斥");

    public static ResCodeEnum getByCode(Integer code) {
        if (code == null) {
            return null;
        }
        ResCodeEnum[] values = ResCodeEnum.values();
        for (ResCodeEnum e : values) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

    private Integer code;
    private String desc;

    ResCodeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
