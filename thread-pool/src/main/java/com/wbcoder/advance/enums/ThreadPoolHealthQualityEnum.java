package com.wbcoder.advance.enums;

import lombok.Getter;

/**
 * 线程池健康状态枚举类
 */
@Getter
public enum ThreadPoolHealthQualityEnum {

    BEST(3, "3级"),
    GOOD(2, "2级"),
    WORSE(1, "1级"),
    WORST(0, "0级");

    public static ThreadPoolHealthQualityEnum getByCode(Integer code) {
        if (code == null) {
            return null;
        }
        ThreadPoolHealthQualityEnum[] values = ThreadPoolHealthQualityEnum.values();
        for (ThreadPoolHealthQualityEnum e : values) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

    private Integer code;
    private String desc;

    ThreadPoolHealthQualityEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
