package com.wbcoder.advance.exception;

import com.wbcoder.advance.enums.ResCodeEnum;
import lombok.Data;

/**
 * 业务异常定义
 */
@Data
public class BusinessException extends RuntimeException {

    private int code;
    private String msg;
    private Exception e;

    public BusinessException(String msg, int code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public BusinessException(String msg, Exception e) {
        super(msg);
        this.msg = msg;
        this.e = e;
    }

    public BusinessException(String msg) {
        super(msg);
        this.msg = msg;
        this.code = 500;
    }

    public BusinessException(int code) {
        super(ResCodeEnum.getByCode(code).getDesc());
        this.msg = ResCodeEnum.getByCode(code).getDesc();
        this.code = code;
    }

    public BusinessException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

    public BusinessException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = 500;
    }

    public static BusinessException throwGlobalException(String msg) {
        return new BusinessException(msg, 500);
    }

    public static BusinessException throwGlobalException(String msg, int code) {
        return new BusinessException(msg, code);
    }

    public static BusinessException throwGlobalException(String msg, Throwable e) {
        return new BusinessException(msg, e);
    }

    public static BusinessException throwGlobalException(String msg, int code, Throwable e) {
        return new BusinessException(msg, code, e);
    }
}
