package com.wbcoder.advance.params;

import lombok.Data;

/**
 * 重新设定线程池参数
 */
@Data
public class ResetThreadPoolParam {

    private Integer corePoolSize;

    private Integer maximumPoolSize;

    private Integer queueCapacity;
}
