package com.wbcoder.advance.pool;

import com.wbcoder.advance.enums.ThreadPoolHealthQualityEnum;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * 自定义线程池，增加健康状况监控
 */
@Slf4j
public class CustomThreadPoolExecutor extends ThreadPoolExecutor {

    /**
     * 线程池健康状况
     * 1：极佳
     * 2：优良
     * 3：欠佳
     */
    private int healthQuality = ThreadPoolHealthQualityEnum.BEST.getCode();

    public CustomThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    public CustomThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
    }

    public CustomThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
    }

    public CustomThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        // 初始化任务状态
        // log.info("before task execute...");
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        // 更新任务执行完成状态
        // log.info("after task execute...");
    }

    public int getHealthQuality() {
        return healthQuality;
    }

    public void setHealthQuality(int healthQuality) {
        this.healthQuality = healthQuality;
    }
}
