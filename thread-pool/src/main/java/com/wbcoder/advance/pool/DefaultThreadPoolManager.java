package com.wbcoder.advance.pool;

/**
 * 默认线程池管理器
 */
public class DefaultThreadPoolManager extends ThreadPoolManager {

    private static ThreadPoolManager threadPool = null;

    public synchronized static ThreadPoolManager getInstance() {
        if (threadPool == null) {
            threadPool = new DefaultThreadPoolManager();
        }
        return threadPool;
    }

    @Override
    protected String getThreadPoolName() {
        return "default-thread-pool";
    }

    @Override
    protected int corePoolSize() {
        return 10;
    }

    @Override
    protected int maximumPoolSize() {
        return 20;
    }
}
