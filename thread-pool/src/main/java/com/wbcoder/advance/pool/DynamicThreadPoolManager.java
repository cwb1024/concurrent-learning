package com.wbcoder.advance.pool;

import com.google.common.collect.Lists;
import com.wbcoder.advance.config.KVConfig;
import com.wbcoder.advance.enums.BizNotifyTypeEnum;
import com.wbcoder.advance.enums.ThreadPoolHealthQualityEnum;
import com.wbcoder.advance.exception.BusinessException;
import com.wbcoder.advance.params.ResetThreadPoolParam;
import com.wbcoder.advance.strategy.biznotify.BizNotifyContext;
import com.wbcoder.advance.strategy.biznotify.BizNotifyHolder;
import com.wbcoder.advance.vo.ThreadPoolStatusInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 动态线程池管理器
 */
@Slf4j
@Component
public class DynamicThreadPoolManager implements InitializingBean {

    @Autowired
    private KVConfig kvConfig;

    @Autowired
    private BizNotifyHolder bizNotifyHolder;

    private final String DEFAULT_THREAD_POOL_NAME = "default-thread-pool";

    /**
     * 线程池映射关系：<线程池名称, 线程池实例>
     */
    private Map<String, CustomThreadPoolExecutor> threadPoolMap = new HashMap<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        CustomThreadPoolExecutor defaultThreadPoolExecutor = new CustomThreadPoolExecutor(5, 10, 0L, TimeUnit.MILLISECONDS, new ResizableLinkedBlockIngQueue<Runnable>(1000));
        threadPoolMap.put(DEFAULT_THREAD_POOL_NAME, defaultThreadPoolExecutor);
        // 启动线程池健康检查
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    for (Map.Entry<String, CustomThreadPoolExecutor> entry : threadPoolMap.entrySet()) {
                        String threadPoolName = entry.getKey();
                        CustomThreadPoolExecutor executor = entry.getValue();
                        int corePoolSize = executor.getCorePoolSize();
                        long taskCount = executor.getTaskCount();
                        int maximumPoolSize = executor.getMaximumPoolSize();
                        int activeCount = executor.getActiveCount();
                        ResizableLinkedBlockIngQueue queue = (ResizableLinkedBlockIngQueue)executor.getQueue();
                        int capacity = queue.getCapacity();
                        int size = queue.size();
                        //log.debug("pool_name={}, core_size={}, max_size={}, active_count={}, queue_capacity={}, queue_size={}, task_count={}", threadPoolName, corePoolSize, maximumPoolSize, activeCount, queue.getCapacity(), queue.size(), taskCount);
                        if (activeCount < corePoolSize) {
                            executor.setHealthQuality(ThreadPoolHealthQualityEnum.BEST.getCode());
                        }
                        if (activeCount > corePoolSize && activeCount < maximumPoolSize) {
                            executor.setHealthQuality(ThreadPoolHealthQualityEnum.GOOD.getCode());
                        }
                        if (activeCount <= maximumPoolSize && size < capacity) {
                            executor.setHealthQuality(ThreadPoolHealthQualityEnum.GOOD.getCode());
                        }
                        if (activeCount >= maximumPoolSize && size < capacity) {
                            executor.setHealthQuality(ThreadPoolHealthQualityEnum.WORSE.getCode());
                        }
                        if (activeCount >= maximumPoolSize && size >= capacity) {
                            executor.setHealthQuality(ThreadPoolHealthQualityEnum.WORST.getCode());
                        }
                    }
                    try {
                        Thread.sleep(kvConfig.dynamicThreadPoolHealthCheckInterval);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }).start();
    }

    /**
     * 添加线程池映射关系
     *
     * @param name
     * @param executor
     */
    public void add(String name, CustomThreadPoolExecutor executor) {
        if (threadPoolMap.get(name) != null) {
            return;
        }
        threadPoolMap.put(name, executor);
    }

    /**
     * 指定线程池执行
     *
     * @param task
     */
    public void execute(String threadPoolName, Runnable task) {
        get(threadPoolName).execute(task);
    }

    /**
     * 不指定线程池：选择一个健康的线程池并执行
     *
     * @param task
     */
    public void execute(Runnable task) {
        String targetThreadPool = null;
        for (Map.Entry<String, CustomThreadPoolExecutor> entry : threadPoolMap.entrySet()) {
            if (entry.getValue().getHealthQuality() == ThreadPoolHealthQualityEnum.BEST.getCode()) {
                targetThreadPool = entry.getKey();
                break;
            }
        }
        if (StringUtils.isEmpty(targetThreadPool)) {
            for (Map.Entry<String, CustomThreadPoolExecutor> entry : threadPoolMap.entrySet()) {
                if (entry.getValue().getHealthQuality() == ThreadPoolHealthQualityEnum.GOOD.getCode()) {
                    targetThreadPool = entry.getKey();
                    break;
                }
            }
        }
        if (StringUtils.isEmpty(targetThreadPool)) {
            for (Map.Entry<String, CustomThreadPoolExecutor> entry : threadPoolMap.entrySet()) {
                if (entry.getValue().getHealthQuality() == ThreadPoolHealthQualityEnum.WORSE.getCode()) {
                    targetThreadPool = entry.getKey();
                    break;
                }
            }
        }
        try {
            threadPoolMap.get(targetThreadPool).execute(task);
        } catch (Exception e) {
            //log.error("线程池任务调度异常, targetThreadPool={}", targetThreadPool, e);
            BizNotifyContext context = new BizNotifyContext();
            if (e instanceof RejectedExecutionException) {
                context.setTitle("线程池任务调度异常: 拒绝投递");
                context.setNotifyType(BizNotifyTypeEnum.COMMON.getCode());
                context.setDetail(targetThreadPool);
                bizNotifyHolder.handle(context);
            } else {
                context.setTitle("线程池任务调度异常: " + e.getLocalizedMessage());
                context.setNotifyType(BizNotifyTypeEnum.COMMON.getCode());
                context.setDetail(targetThreadPool);
                bizNotifyHolder.handle(context);
            }
        }
    }

    /**
     * 添加线程池映射关系
     *
     * @param name
     */
    public ThreadPoolExecutor get(String name) {
        CustomThreadPoolExecutor threadPoolExecutor = threadPoolMap.get(name);
        if (threadPoolExecutor == null) {
            return threadPoolMap.get(DEFAULT_THREAD_POOL_NAME);
        }
        return threadPoolExecutor;
    }

    /**
     * 返回默认线程池
     *
     * @return
     */
    public CustomThreadPoolExecutor get() {
        return threadPoolMap.get(DEFAULT_THREAD_POOL_NAME);
    }

    /**
     * 移除线程池映射关系
     *
     * @param name
     */
    public void remove(String name) {
        if (threadPoolMap.get(name) == null) {
            return;
        }
        threadPoolMap.remove(name);
    }

    /**
     * 线程池实例重置
     *
     * @param name
     * @param param
     */
    public synchronized void reset(String name, ResetThreadPoolParam param) {
        ThreadPoolExecutor threadPoolExecutor = threadPoolMap.get(name);
        if (threadPoolExecutor == null) {
            return;
        }
        Integer corePoolSize = param.getCorePoolSize();
        Integer maximumPoolSize = param.getMaximumPoolSize();
        Integer queueCapacity = param.getQueueCapacity();
        if (corePoolSize != null && corePoolSize <= 0) {
            throw new BusinessException("核心线程数配置非法");
        }
        if (maximumPoolSize != null && maximumPoolSize <= 0) {
            throw new BusinessException("最大线程数配置非法");
        }
        if (queueCapacity != null && queueCapacity <= 0) {
            throw new BusinessException("队列容量配置非法");
        }
        if (corePoolSize != null && maximumPoolSize != null && corePoolSize > maximumPoolSize) {
            throw new BusinessException("核心线程数不能大于最大线程数");
        }
        if (corePoolSize != null) {
            threadPoolExecutor.setCorePoolSize(corePoolSize);
        }
        if (maximumPoolSize != null) {
            threadPoolExecutor.setMaximumPoolSize(maximumPoolSize);
        }
        if (queueCapacity != null && queueCapacity > 0 && threadPoolExecutor.getQueue() instanceof ResizableLinkedBlockIngQueue) {
            ResizableLinkedBlockIngQueue resizableLinkedBlockIngQueue = (ResizableLinkedBlockIngQueue) threadPoolExecutor.getQueue();
            resizableLinkedBlockIngQueue.setCapacity(queueCapacity);
        }
    }

    /**
     * @description:获取线程池名称
     * @author:dongchenxu
     * @date:2021/4/19 3:31 下午
     */
    public List<String> getThreadPoolName() {
        return Lists.newArrayList(this.threadPoolMap.keySet());
    }

    public List<ThreadPoolStatusInfo> getThreadPoolStatus() {
        Map<String, CustomThreadPoolExecutor> threadPoolMap = this.threadPoolMap;
        List<ThreadPoolStatusInfo> data = Lists.newArrayListWithCapacity(threadPoolMap.size());
        for (Map.Entry<String, CustomThreadPoolExecutor> entry : threadPoolMap.entrySet()) {
            ThreadPoolStatusInfo threadPoolStatusInfo = new ThreadPoolStatusInfo();
            String threadPoolName = entry.getKey();
            CustomThreadPoolExecutor executor = entry.getValue();
            threadPoolStatusInfo.setPool_name(threadPoolName);
            threadPoolStatusInfo.setCore_thread_size(executor.getCorePoolSize());
            threadPoolStatusInfo.setMax_thread_size(executor.getMaximumPoolSize());
            threadPoolStatusInfo.setActive_thread_size(executor.getActiveCount());
            if (executor.getQueue() instanceof ResizableLinkedBlockIngQueue) {
                ResizableLinkedBlockIngQueue resizableLinkedBlockIngQueue = (ResizableLinkedBlockIngQueue)executor.getQueue();
                threadPoolStatusInfo.setQueue_type("ResizableLinkedBlockIngQueue");
                threadPoolStatusInfo.setQueue_capacity(resizableLinkedBlockIngQueue.getCapacity());
                threadPoolStatusInfo.setQueue_size(resizableLinkedBlockIngQueue.size());
                threadPoolStatusInfo.setHealth_quality(executor.getHealthQuality());
                threadPoolStatusInfo.setTask_count(executor.getTaskCount());
                threadPoolStatusInfo.setCompleted_task_count(executor.getCompletedTaskCount());
                data.add(threadPoolStatusInfo);
            }
        }
        return data;
    }
}
