package com.wbcoder.advance.pool;


import java.util.concurrent.*;


public class ExecutorServiceThreadPool {

    /**
     * 线程池默认名称
     */
    private static final String DEFAULT_NAME = "default-thread-pool-executor";
    /**
     * 核心线程数大小默认值
     */
    private static final int DEFAULT_CORE_SIZE = 4;
    /**
     * 最大线程数默认值
     */
    private static final int DEFAULT_MAX_SIZE = 4;
    /**
     * 任务队列最大数量
     */
    private static final int DEFAULT_MAX_QUEUE_SIZE = 200000;

    private static ThreadPoolExecutor threadPoolExecutor = null;

    private static ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat(getThreadPoolName()+"-%d").build();

    public static ExecutorService getThreadPoolExecutor() {
        if (threadPoolExecutor == null){
            threadPoolExecutor = new ThreadPoolExecutor(
                    corePoolSize(), maximumPoolSize(),0, TimeUnit.SECONDS, new LinkedBlockingQueue<>(maxQueueSize()), threadFactory,new ThreadPoolExecutor.AbortPolicy());
        }
        return threadPoolExecutor;
    }

    protected static String getThreadPoolName() {
        return DEFAULT_NAME;
    }

    protected static int corePoolSize() {
        return DEFAULT_CORE_SIZE;
    }

    protected static int maximumPoolSize() {
        return DEFAULT_MAX_SIZE;
    }

    protected static int maxQueueSize(){return DEFAULT_MAX_QUEUE_SIZE;}
}
