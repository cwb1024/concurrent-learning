package com.wbcoder.advance.pool;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * 调度线程池
 */
public class ScheduledExecutorService {

    private volatile static ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = null;

    private static Object lock = new Object();

    /**
     * 获取单例的线程池对象
     */
    public static ScheduledThreadPoolExecutor getThreadPoolInstance() {
        if (scheduledThreadPoolExecutor == null) {
            synchronized (lock) {
                if (scheduledThreadPoolExecutor == null) {
                    scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(15, new BasicThreadFactory.Builder().namingPattern("example-schedule-pool-%d").daemon(true).build());
                }
            }
        }
        return scheduledThreadPoolExecutor;
    }
}
