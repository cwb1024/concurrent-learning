package com.wbcoder.advance.pool;

import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfigChangeListener;
import com.wbcoder.advance.params.ResetThreadPoolParam;
import com.wbcoder.advance.util.JacksonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * 线程池配置参数变更监听
 */
@Slf4j
@Service
public class ThreadPoolConfigModifyWatcher {

    @Autowired
    private DynamicThreadPoolManager dynamicThreadPoolManager;

    @ApolloConfigChangeListener(value = {"thread-pool"})
    public void watchConfigChange(ConfigChangeEvent changeEvent) {
        Set<String> changedKeys = changeEvent.changedKeys();
        for (String threadPoolName : changedKeys) {
            String threadPoolConfig = changeEvent.getChange(threadPoolName).getNewValue();
            ResetThreadPoolParam param = JacksonUtil.json2Bean(threadPoolConfig, ResetThreadPoolParam.class);
            try {
                dynamicThreadPoolManager.reset(threadPoolName, param);
            } catch (Exception e) {
                log.error("监听线程池配置变更重置线程池实例发生异常", e);
            }
        }
    }
}
