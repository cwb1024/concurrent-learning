package com.wbcoder.advance.strategy.biznotify;

import com.wbcoder.advance.exception.BusinessException;
import lombok.Data;

@Data
public class BizNotifyContext {

    private String notifyType;
    private String title;
    private String path;
    private String detail;
    private String requestParams;
    private Exception unknownException;
    private BusinessException bizException;

}
