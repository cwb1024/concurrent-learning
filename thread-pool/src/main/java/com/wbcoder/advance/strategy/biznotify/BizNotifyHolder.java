package com.wbcoder.advance.strategy.biznotify;

import com.alibaba.fastjson.JSON;
import com.wbcoder.advance.enums.BizNotifyTypeEnum;
import com.wbcoder.advance.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 业务通知持有者
 */
@Slf4j
@Component
public class BizNotifyHolder implements InitializingBean {

    private Map<String, BizNotifyStrategy> map = new HashMap<>();

    @Autowired
    private DingDingNotifyProvider dingDingNotifyProvider;

    @Override
    public void afterPropertiesSet() {
        map.put(BizNotifyTypeEnum.DING_DING.getCode(), dingDingNotifyProvider);
    }

    /**
     * 处理
     * @param context
     */
    public void handle(BizNotifyContext context) {
        if (context == null || StringUtils.hasLength(context.getNotifyType())) {
            String message = "BizNotifyHolder.handle fail. msg=引擎类型为空. context=" + JSON.toJSONString(context);
            throw new BusinessException(message);
        }
        BizNotifyStrategy strategy = map.get(context.getNotifyType());
        if (strategy == null) {
            String message = "BizNotifyHolder.handle fail. msg=未找到处理策略. context=" + JSON.toJSONString(context);
            throw new BusinessException(message);
        }
        strategy.alarm(context);
    }
}
