package com.wbcoder.advance.strategy.biznotify;

import com.wbcoder.advance.config.KVConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * 业务通知策略抽象类
 */
@Slf4j
@Service
public abstract class BizNotifyStrategy {

    @Autowired
    private KVConfig kvConfig;

    public abstract void alarm(BizNotifyContext context);

    /**
     * 获取本机ip地址
     *
     * @return
     */
    public String getHostIp() {
        String tempIp = "127.0.0.1";
        try {
            tempIp = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            //log.info("getHostIP error.", e);
        }
        try {
            Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
            InetAddress ip = null;
            Enumeration<InetAddress> addrs;
            while (networks.hasMoreElements()) {
                addrs = networks.nextElement().getInetAddresses();
                while (addrs.hasMoreElements()) {
                    ip = addrs.nextElement();
                    if (ip != null && ip instanceof Inet4Address && ip.isSiteLocalAddress() && !ip.getHostAddress().equals(tempIp)) {
                        return ip.getHostAddress();
                    }
                }
            }
            return tempIp;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取异常详情
     *
     * @param e
     * @return
     */
    public String getLocation(Exception e) {
        if (e == null) {
            return null;
        }
        StringBuilder stackTraceBuffer = new StringBuilder();
        for (StackTraceElement item : e.getStackTrace()) {
            if (kvConfig.switchNotifyAllTraceShow) {
                stackTraceBuffer.append(item.getClassName()).append(".").append(item.getMethodName()).append(item.getLineNumber()).append("\n");
            } else {
                if (item.getClassName().startsWith("com.wbcoder")) {
                    stackTraceBuffer.append(item.getClassName()).append(".").append(item.getMethodName()).append(item.getLineNumber()).append("\n");
                }
            }
        }
        return stackTraceBuffer.toString();
    }


}
