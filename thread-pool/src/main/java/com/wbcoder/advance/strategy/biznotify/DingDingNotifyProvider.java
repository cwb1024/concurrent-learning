package com.wbcoder.advance.strategy.biznotify;

import com.alibaba.fastjson.JSONObject;
import com.wbcoder.advance.config.KVConfig;
import com.wbcoder.advance.pool.DefaultThreadPoolManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 钉钉通知提供者
 */
@Slf4j
@Service
public class DingDingNotifyProvider extends BizNotifyStrategy {

    @Autowired
    private KVConfig kvConfig;
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void alarm(BizNotifyContext context) {
        Long timestamp = System.currentTimeMillis();
        String url = kvConfig.dingdingUrl + "&timestamp=" + timestamp + "&sign=" + getSign(timestamp, kvConfig.dingDingSecretKey);
        URI uri = null;
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            //log.error("zhiyinlou talk uri error,url:{}", url, e);
            throw new RuntimeException("zhiyinlou talk uri to url error");
        }

        JSONObject json = new JSONObject();
        json.put("msgtype", "text");
        JSONObject js = new JSONObject();
        js.put("content", buildSendParams(context));
        json.put("text", js);
        JSONObject atJs = new JSONObject();
        atJs.put("isAtAll", true);
        json.put("at", atJs);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity httpEntity = new HttpEntity(json.toJSONString(), httpHeaders);
        restTemplate.postForEntity(uri, httpEntity, String.class);
    }

    /**
     * 签名
     *
     * @param timestamp
     * @param secret
     * @return
     */
    private String getSign(Long timestamp, String secret) {
        try {
            String stringToSign = timestamp + "\n" + secret;
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256"));
            byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
            return URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 构建消息发送模版
     *
     * @param context
     * @return
     */
    public String buildSendParams(BizNotifyContext context) {
        String title = context.getTitle();
        String path = context.getPath();
        String requestParams = context.getRequestParams();
        //获取当前时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String now = sdf.format(date);
        //封装消息内容
        StringBuffer messageBuffer = new StringBuffer();
        messageBuffer.append("\n标题: ").append(title).append("\n");
        messageBuffer.append("接口: ").append(path).append("\n");
        String detail = context.getDetail();
        if (StringUtils.hasLength(detail)) {
            if (context.getBizException() != null) {
                detail = getLocation(context.getBizException());
                messageBuffer.append("详情: ").append(context.getBizException().getMessage()).append("\n");
            } else if (context.getUnknownException() != null) {
                detail = getLocation(context.getUnknownException());
                messageBuffer.append("详情: ").append(context.getUnknownException().getMessage()).append("\n");
            }
        }
        messageBuffer.append("位置: ").append(detail).append("\n");
        messageBuffer.append("参数: ").append(requestParams).append("\n");
        messageBuffer.append("\n服务: ").append(kvConfig.appId).append("\n");
        messageBuffer.append("机器: ").append(getHostIp()).append("\n");
        messageBuffer.append("时间: ").append(now).append("\n");

        return messageBuffer.toString();
    }

    /**
     * 是否阻塞发送
     *
     * @param appEnv
     * @return
     */
    private boolean blockSend(String appEnv) {
        if (StringUtils.hasLength(appEnv)) {
            // 当前环境和目标环境匹配，则不阻塞
            if (kvConfig.appEnv.equals(appEnv)) {
                return false;
            }
            return true;
        } else {
            // 发送信息支持环境中包含当前环境，则不阻塞
            String commonSendEnvList = kvConfig.commonSendEnvList;
            if (commonSendEnvList.contains(kvConfig.appEnv)) {
                return false;
            }
            return true;
        }
    }

    /**
     * 一般发送
     *
     * @param webhook
     * @param secret
     * @param content
     * @param appEnv
     */
    public void commonSend(String webhook, String secret, String content, String appEnv) {
        if (blockSend(appEnv)) {
            return;
        }
        DefaultThreadPoolManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                Long timestamp = System.currentTimeMillis();
                String url = webhook + "&timestamp=" + timestamp + "&sign=" + getSign(timestamp, secret);
                URI uri = null;
                try {
                    uri = new URI(url);
                } catch (URISyntaxException e) {
                    //log.error("dingding talk uri error,url:{}", url, e);
                    throw new RuntimeException("zhiyinlou talk uri to url error");
                }
                JSONObject json = new JSONObject();
                json.put("msgtype", "text");
                JSONObject js = new JSONObject();
                js.put("content", content);
                json.put("text", js);
                JSONObject atJs = new JSONObject();
                atJs.put("isAtAll", true);
                json.put("at", atJs);
                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
                HttpEntity httpEntity = new HttpEntity(json.toJSONString(), httpHeaders);
                restTemplate.postForEntity(uri, httpEntity, String.class);
            }
        });

    }
}
