package com.wbcoder.advance.util;

/**
 * 数组工具类
 */
public class ArrayUtils {

    /**
     * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
     *
     * @param nums
     * @param target
     * @return
     */
    public static int searchInsertIndex(double[] nums, double target) {
        if (nums.length == 0) return 0;
        if (nums[0] > target) return 0;
        if (nums[nums.length - 1] < target) return nums.length;
        int start = 0;
        int end = nums.length - 1;
        while (start <= end) {
            int middle = ((end - start) >> 1) + start;
            if (nums[middle] == target) {
                return middle == 0 ? 0 : middle;
            } else if (nums[middle] > target) {
                if (middle - 1 >= 0 && nums[middle - 1] < target) {
                    return middle;
                }
                end = middle - 1;
            } else {
                if (middle + 1 < nums.length && nums[middle + 1] > target) {
                    return middle + 1;
                }
                start = middle + 1;
            }
        }
        return 0;
    }
}
