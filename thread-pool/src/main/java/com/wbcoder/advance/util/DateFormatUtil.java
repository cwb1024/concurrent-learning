package com.wbcoder.advance.util;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 日期格式化工具类
 */
@Slf4j
public class DateFormatUtil {

    /**
     * yyyyMMdd
     */
    public static final String YMD = "yyyyMMdd";
    /**
     * yyyyMM
     */
    public static final String YM = "yyyyMMd";
    /**
     * yyyy/MM/dd
     */
    public static final String YMD_SLASH = "yyyy/MM/dd";
    /**
     * yyyy-MM-dd
     */
    public static final String YMD_DASH = "yyyy-MM-dd";
    /**
     * yyyy-MM-dd H:m
     */
    public static final String YMD_DASH_WITH_TIME = "yyyy-MM-dd HH:mm:ss";
    /**
     * yyyyMMddHHmmss
     */
    public static final String YMD_DASH_WITH_TIME_SIMPLE = "yyyyMMddHHmmss";
    /**
     * yyyyMMddHHmmss
     */
    public static final String YMD_DASH_WITH_TIME_SLANT = "yyyy/MM/dd/HH/mm/ss";
    /**
     * yyyy/dd/MM
     */
    public static final String YDM_SLASH = "yyyy/dd/MM";
    /**
     * yyyy-dd-MM
     */
    public static final String YDM_DASH = "yyyy-dd-MM";
    /**
     * HHmm
     */
    public static final String HM = "HHmm";
    /**
     * HH:mm
     */
    public static final String HM_COLON = "HH:mm";
    /**
     * 24 * 60 * 60 * 1000L
     */
    public static final long DAY = 24 * 60 * 60 * 1000L;
    /**
     * 24 * 60 * 60 * 1000L
     */
    public static final long HOUR = 60 * 60 * 1000L;
    /**
     * 1000L
     */
    public static final long SECOND = 1000L;

    private DateFormatUtil() {
    }

    private static final Object LOCK_OBJ = new Object();

    private static Map<String, ThreadLocal<SimpleDateFormat>> sdfMap = new ConcurrentHashMap<String, ThreadLocal<SimpleDateFormat>>();

    /**
     * 获取日期格式对象
     *
     * @param pattern 日期格式
     */
    private static SimpleDateFormat getFormat(final String pattern) {
        ThreadLocal<SimpleDateFormat> tl = sdfMap.get(pattern);
        if (tl != null) {
            return tl.get();
        }
        synchronized (LOCK_OBJ) {
            final ThreadLocal<SimpleDateFormat> simpleDateFormatThreadLocal = ThreadLocal.withInitial(() -> {
                return new SimpleDateFormat(pattern);
            });
            sdfMap.computeIfAbsent(pattern, key -> simpleDateFormatThreadLocal);
            return simpleDateFormatThreadLocal.get();
        }
    }

    /**
     * 日期字符串按照日期格式转换为日期类型
     *
     * @param pattern 日期格式
     */
    public static Date parse(String source, String pattern) throws Exception {
        if (source == null) {
            return null;
        }
        Date date;
        try {
            date = getFormat(pattern).parse(source);
        } catch (ParseException e) {
            throw new Exception(source + " doesn't match " + pattern);
        }
        return date;
    }

    /**
     * @description: 将日期格式化为指定格式
     */
    public static String format(Date date, String pattern) throws Exception {
        if (date == null) {
            throw new Exception("params is illegal");
        }
        return getFormat(pattern).format(date);
    }

    /**
     * @description: 将日期格式化为默认格式
     * 默认格式为 yyyy-MM-dd HH:mm:ss
     */
    public static String formatDefaultPattern(Date date) throws Exception {
        if (date == null) {
            throw new Exception("params is illegal");
        }
        return getFormat(YMD_DASH_WITH_TIME).format(date);
    }
}
