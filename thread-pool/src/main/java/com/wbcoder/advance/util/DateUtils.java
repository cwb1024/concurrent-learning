package com.wbcoder.advance.util;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * 日期工具类
 */
@Slf4j
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

    public static final String FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private DateUtils() throws RuntimeException {
        throw new RuntimeException("工具类禁止实例化");
    }

    /**
     * 获取上周的开始时刻
     */
    public static Date getBeginTimeOfLastWeek() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayofweek = cal.get(Calendar.DAY_OF_WEEK);
        if (dayofweek == 1) {
            dayofweek += 7;
        }
        cal.add(Calendar.DATE, 2 - dayofweek - 7);
        return getDayStartTime(cal.getTime());
    }

    /**
     * 获取上周的结束时刻
     */
    public static Date getEndTimeOfLastWeek() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getBeginTimeOfLastWeek());
        cal.add(Calendar.DAY_OF_WEEK, 6);
        return getDayEndTime(cal.getTime());
    }

    /**
     * 获取本周的开始时刻
     */
    public static Date getBeginTimeOfCurrentWeek() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayofweek = cal.get(Calendar.DAY_OF_WEEK);
        if (dayofweek == 1) {
            dayofweek += 7;
        }
        cal.add(Calendar.DATE, 2 - dayofweek);
        return getDayStartTime(cal.getTime());
    }

    /**
     * 获取本周的结束时刻
     */
    public static Date getEndTimeOfCurrentWeek() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getBeginTimeOfLastWeek());
        cal.add(Calendar.DAY_OF_WEEK, 13);
        return getDayEndTime(cal.getTime());
    }

    private static Calendar convert(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * 获取一天的开始时刻
     *
     * @param date
     * @return
     */
    public static Date getDayStartTime(Date date) {
        Calendar cal = convert(date);
        int yy = cal.get(Calendar.YEAR);
        int MM = cal.get(Calendar.MONTH);
        int dd = cal.get(Calendar.DATE);
        Calendar cal1 = Calendar.getInstance();
        cal1.set(yy, MM, dd, 00, 00, 00);
        return cal1.getTime();
    }

    /**
     * 获取一天的结束时刻
     *
     * @param date
     * @return
     */
    public static Date getDayEndTime(Date date) {
        Calendar cal = convert(date);
        int yy = cal.get(Calendar.YEAR);
        int MM = cal.get(Calendar.MONTH);
        int dd = cal.get(Calendar.DATE);
        Calendar cal1 = Calendar.getInstance();
        cal1.set(yy, MM, dd, 23, 59, 59);
        return cal1.getTime();
    }

    /**
     * 获取两个日期相差的天数
     */
    public static int daysBetween(Date date1, Date date2) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        long time1 = cal.getTimeInMillis();
        cal.setTime(date2);
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);
        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * 获取上周六的凌晨时间
     */
    public static Date getLastSaturdayStartTime() {
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.DAY_OF_WEEK, 7);
        instance.add(Calendar.WEEK_OF_YEAR, -1);
        return getDayStartTime(instance.getTime());
    }

    /**
     * 获取本周五的结束时间
     */
    public static Date getCurrentFivdayEndTime() {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() + 4);
        return getDayEndTime(cal.getTime());
    }

    /**
     * 获取下周五的结束时刻
     */
    public static Date getNextFivdayEndTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, 7);
        cal.add(Calendar.WEEK_OF_YEAR, 1);
        return getDayStartTime(cal.getTime());
    }

    /**
     * 日期转字符串
     */
    public static String date2Str(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * 时间格式化
     *
     * @param seconds
     * @return
     */
    public static String formatTime(int seconds) {
        if (seconds == 0) {
            return "0秒";
        }
        Date date = new Date(seconds * 1000);
        SimpleDateFormat df = new SimpleDateFormat("HH.mm.ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT+0:00"));
        String format = df.format(date);
        String[] split = format.split("\\.");
        StringBuilder sb = new StringBuilder();
        String hour = split[0];
        String minute = split[1];
        String second = split[2];
        if (!hour.equals("00")) {
            if (hour.startsWith("0")) {
                hour = hour.substring(1);
            }
            sb.append(hour).append("时");
        }
        if (!minute.equals("00")) {
            if (minute.startsWith("0")) {
                minute = minute.substring(1);
            }
            sb.append(minute).append("分");
        }
        if (!second.equals("00")) {
            if (second.startsWith("0")) {
                second = second.substring(1);
            }
            sb.append(second).append("秒");
        }
        return sb.toString();
    }

}

