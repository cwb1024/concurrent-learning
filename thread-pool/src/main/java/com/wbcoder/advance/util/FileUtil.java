package com.wbcoder.advance.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * 文件操作工具类
 */
@Slf4j
public class FileUtil {

    /**
     * @param url
     * @param filePath
     * @throws Exception
     */
    public static void download(String url, String filePath) throws Exception {
        // 下载网络文件
        int bytesum = 0;
        int byteread = 0;
        URL netUrl = new URL(url);
        URLConnection conn = netUrl.openConnection();
        try (InputStream inStream = conn.getInputStream();
             FileOutputStream fs = new FileOutputStream(filePath)) {
            byte[] buffer = new byte[1204];
            while ((byteread = inStream.read(buffer)) != -1) {
                bytesum += byteread;
                fs.write(buffer, 0, byteread);
            }
        } catch (Exception e) {
            //log.error("download file error", e);
        }
    }


    /**
     * 将InputStream转为本地文件
     *
     * @param inStream
     * @param filePath
     * @throws Exception
     */
    public static void inputStreamToFile(InputStream inStream, String filePath) throws Exception {
        int bytesum = 0;
        int byteread = 0;
        try (FileOutputStream fs = new FileOutputStream(filePath);) {
            byte[] buffer = new byte[1204];
            while ((byteread = inStream.read(buffer)) != -1) {
                bytesum += byteread;
                fs.write(buffer, 0, byteread);
            }
        } catch (Exception e) {
            //log.error("inputStreamToFile exception : ",e);
            throw e;
        } finally {
            if (inStream != null) {
                inStream.close();
            }
        }
    }

    /**
     * 获取文件扩展名
     * @param file
     * @return
     */
    public static String getFileExtension(MultipartFile file) {
        if (file == null) {
            return null;
        }
        String originalFilename = file.getOriginalFilename();
        return getFileExtension(originalFilename);
    }

    /**
     * 获取文件扩展名
     *
     * @param filePath
     * @return
     */
    public static String getFileExtension(String filePath) {
        if (StringUtils.isBlank(filePath)) {
            return null;
        }
        int dotIndex = filePath.lastIndexOf(".");
        if (dotIndex <= 0) {
            return null;
        }
        return filePath.substring(dotIndex);
    }

    /**
     * 删除本地临时文件
     *
     * @param localFilePath
     */
    public static void deleteFile(String localFilePath) {
        try {
            File file = new File(localFilePath);
            file.delete();
        } catch (Exception e) {
            //log.error("删除本地临时文件异常", e);
        }
    }
}