package com.wbcoder.advance.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.beans.BeanCopier;

import java.util.List;
import java.util.Map;

/**
 * Jackson工具类
 */
@Slf4j
public class JacksonUtil {

    private static ObjectMapper mapper = new ObjectMapper();

    public static ArrayNode createArrayNode() {
        ArrayNode node;
        try {
            node = mapper.createArrayNode();
        } catch (Exception e) {
            log.error("jackson create node error.");
            return null;
        }
        return node;
    }

    public static ObjectNode createNode() {
        ObjectNode node;
        try {
            node = mapper.createObjectNode();
        } catch (Exception e) {
            log.error("jackson create node error.");
            return null;
        }
        return node;
    }

    /**
     * bean对象之间值拷贝
     */
    public static void copy(Object source, Object destinationObject) {
        if (destinationObject == null || source == null) {
            return;
        }
        BeanCopier copier = BeanCopier.create(source.getClass(), destinationObject.getClass(), false);
        copier.copy(source, destinationObject, null);
    }

    public static String bean2Json(Object data) {
        try {
            String result = mapper.writeValueAsString(data);
            return result;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T json2Bean(String jsonData, Class<T> beanType) {
        try {
            T result = mapper.readValue(jsonData, beanType);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T> List<T> json2List(String jsonData, Class<T> beanType) {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        JavaType javaType = mapper.getTypeFactory().constructParametricType(List.class, beanType);

        try {
            List<T> resultList = mapper.readValue(jsonData, javaType);
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <K, V> Map<K, V> json2Map(String jsonData, Class<K> keyType, Class<V> valueType) {
        JavaType javaType = mapper.getTypeFactory().constructMapType(Map.class, keyType, valueType);

        try {
            Map<K, V> resultMap = mapper.readValue(jsonData, javaType);
            return resultMap;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static JsonNode parse2json(String jsonData) {
        try {
            JsonNode jsonNode = mapper.readTree(jsonData);
            return jsonNode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ObjectNode createObjectNode() {
        ObjectNode objectNode = mapper.createObjectNode();
        return objectNode;
    }
}
