package com.wbcoder.advance.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 对象拷贝工具类
 */
@Slf4j
public class ObjectCopyUtil {

    public static <B> B copyProperties(Object source, Class<B> targetCls) throws InstantiationException, IllegalAccessException {
        B target = targetCls.newInstance();
        BeanUtils.copyProperties(source, target);
        return target;
    }

    public static <A, B> List<B> copyListProperties(List<A> sourceList, Class<B> targetCls) throws InstantiationException, IllegalAccessException {
        List<B> targetList = new ArrayList<>();
        for (A source : sourceList) {
            targetList.add(copyProperties(source, targetCls));
        }
        return targetList;
    }

    @Deprecated
    public static void copyExcludeNull(Object source, Object target) throws ClassNotFoundException, IllegalAccessException, NoSuchFieldException, ParseException {
        if (source == null || target == null) return;

        Class<?> tClazz = Class.forName(target.getClass().getName());
        Class<?> sClazz = Class.forName(source.getClass().getName());
        Field[] tFields = tClazz.getDeclaredFields();
        Field[] sFields = sClazz.getDeclaredFields();

        if (tFields == null || tFields.length == 0 || sFields == null || sFields.length == 0) return;

        for (Field tField : tFields) {
            tField.setAccessible(true);
            for (Field sField : sFields) {
                if (tField.getName().equals(sField.getName())) {
                    sField.setAccessible(true);
                    setValue(tClazz, target, tField.getName(), sField.get(source));
                }
            }
        }

        tClazz = tClazz.getSuperclass();
        tFields = tClazz.getDeclaredFields();
        for (Field tField : tFields) {
            tField.setAccessible(true);
            for (Field sField : sFields) {
                if (tField.getName().equals(sField.getName())) {
                    sField.setAccessible(true);
                    setValue(tClazz, target, tField.getName(), sField.get(source));
                }
            }
        }
    }

    private static void setValue(Class<?> clazz, Object obj, String fieldName, Object val) throws NoSuchFieldException, IllegalAccessException, ParseException {
        String value = "";
        if (val != null) value = val.toString();
        Field field = clazz.getDeclaredField(fieldName);
        field.setAccessible(true);
        if (val == null) {
            return;
        }
        Class<?> type = field.getType();
        if (StringUtils.isNotBlank(value)) {
            if (type.equals(Integer.class))
                field.set(obj, Integer.valueOf(value));
            else if (type.equals(Double.class))
                field.set(obj, Double.valueOf(value));
            else if (type.equals(Float.class))
                field.set(obj, Float.valueOf(value));
            else if (type.equals(Character.class))
                field.set(obj, Character.valueOf(value.charAt(0)));
            else if (type.equals(Date.class)) {
                if (value != null && !value.equals("")) {
                    String format = value.length() == 10 ? "yyyy-MM-dd" : "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat sdf = new SimpleDateFormat(format);
                    field.set(obj, sdf.parse(value));
                }
            } else if (type.equals(String.class))
                field.set(obj, value);
            else if (type.equals(Long.class))
                field.set(obj, Long.valueOf(value));
            else if (type.equals(Boolean.class))
                field.set(obj, (Boolean) val);
        } else {
            if (type.equals(String.class))
                field.set(obj, value);
        }
    }
}
