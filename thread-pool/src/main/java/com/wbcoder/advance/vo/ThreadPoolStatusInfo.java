package com.wbcoder.advance.vo;

import lombok.Data;

/**
 * 线程池状态信息
 */
@Data
public class ThreadPoolStatusInfo {

    private String pool_name;

    private int health_quality;

    private int core_thread_size;
    private int max_thread_size;
    private int active_thread_size;

    private String queue_type;
    private int queue_size;
    private int queue_capacity;

    private long task_count;
    private long completed_task_count;

}
