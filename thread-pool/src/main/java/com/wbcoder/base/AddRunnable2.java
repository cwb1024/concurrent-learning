package com.wbcoder.base;

import java.util.concurrent.CopyOnWriteArrayList;

/**
  * @description: 这里怎么模拟抢一个锁的情况
  * @author: chengwb
  * @Date: 2020-01-05 04:50
  */
public class AddRunnable2 implements Runnable {

    private CopyOnWriteArrayList copyOnWriteArrayList = null;

    private Object data;

    public AddRunnable2(){
    }

    @Override
    public void run() {

        System.out.println("time:" + System.currentTimeMillis() +",AddRunnable2,开始执行加数据。。。。");



        try {
            Thread.sleep(5000);
            data = 100;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("time:" + System.currentTimeMillis() +",AddRunnable2,加数据执行完成。。。。");

    }
}
