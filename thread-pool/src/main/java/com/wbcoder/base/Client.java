package com.wbcoder.base;

import java.util.concurrent.*;

/**
  * @description: 这里搞一个线程池，需要区分清楚怎么设计一个任务，任务的数据怎么分配，怎么用池子开启多个线程来管理任务的执行
  * @author: chengwb
  * @Date: 2020-01-05 01:37
  */
public class Client {

    public static void main(String[] args) {

        // 这里初始化一个线程池

        // todo 初始化线程池的方式有哪些

        // 利用JDK提供好的api

        ExecutorService executorService = Executors.newCachedThreadPool();


        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();


        ExecutorService executorService1 = Executors.newSingleThreadExecutor();

        BlockingDeque blockingDeque = new LinkedBlockingDeque();

        ThreadPoolExecutor threadPoolExecutor;
        threadPoolExecutor = new ThreadPoolExecutor(20, 100, 10000, TimeUnit.SECONDS, blockingDeque);

        Future<?> submit = threadPoolExecutor.submit(new AddRunnable());

        Future<?> submit1 = threadPoolExecutor.submit(new AddRunnable1());

        Future<?> submit2 = threadPoolExecutor.submit(new AddRunnable2());

        try {
            System.out.println("time:" + System.currentTimeMillis() +",开始等待获取值。。。");
            //todo 这里任务没有执行完，这里就是阻塞的，这里需要轮询
            System.out.println(submit.get());
            System.out.println(submit1.get());
            System.out.println(submit2.get());
            System.out.println("time:" + System.currentTimeMillis() +",获取值完毕。。。");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

//        threadPoolExecutor.execute(null);

        System.out.println(threadPoolExecutor.getActiveCount());

        System.out.println(threadPoolExecutor.getCorePoolSize());

        System.out.println(threadPoolExecutor.allowsCoreThreadTimeOut());


    }
}
