package com.wbcoder.callable;

import java.util.concurrent.*;

/**
 * @description: 需求：主线程需要等待子线程把任务并行的执行完成，需要把任务拆分出来进行执行完成
 * 需要关注子线程的执行结果，对子线程的结果进行响应，如果子线程出现异常、中断需要对子线程进行响应，这个时候需要
 * 中断整个任务线程的执行。
 * 需要关注子线程的响应结果，JDK提供的可以关注子线程返回结果的方法，这里需要研究下
 * 利用Thread、Runnable这里只会把任务命令派发给子线程，而不会关注子线程的执行结果，没有办法对结果进行响应
 * 这里需要研究下JDK的callable、future、futureTask
 * <p>
 * 这里可以向线程池内提交一个任务，那么如果提交多个任务的时候，这几个任务之间是怎么协同的呢？
 * <p>
 * 如果其中一个任务发生了异常，这里采取的策略是放弃所有的任务
 * @author: chengwb
 * @Date: 2020/5/24 13:10
 */
public class CallableClient {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        Future<Integer> future = executor.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                System.out.println("子任务开始执行。。");
                Thread.sleep(3000);
                int sum = 0;
                if (sum == 0) {
                    throw new Exception("这里抛出来一个异常，主线程需要怎么处理呢？");
                }
                return ++sum;
            }
        });

        System.out.println("主线程等待子线程的执行结果。。。");
        Integer integer = null;
        try {
            integer = future.get();
        } catch (Exception e) {
            System.out.println("等待子线程执行任务的过程中发生了异常，这里需要进行处理下");
            //这里准备中断所有的已经执行的任务
            executor.shutdown();
        }

        System.out.println("子线程执行结果等待完毕。。。");
        System.out.println(integer);

        executor.shutdown();
    }
}
