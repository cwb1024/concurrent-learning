package com.wbcoder.callable;

import com.wbcoder.model.TaskBean;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @description: 利用futureTask进行异步统计
 * <p>
 * FutureTask弥补了Future必须用线程池提交返回Future的缺陷
 * <p>
 * 1.Runnable接口，可开启线程执行。
 * 2.Future<v>接口，可接受Callable接口的返回值，futureTask.get()阻塞获取结果。
 * 3.这两个步骤：一个开启线程执行任务，一个阻塞等待执行结果，分离这两步骤，可在这两步中间穿插别的相关业务逻辑。
 * @author: chengwb
 * @Date: 2020/5/24 14:48
 */
public class FutureTaskClient {

    public static void main(String[] args) throws InterruptedException, ExecutionException {


        //这里搞一个线程让子线程取执行任务,也可以用线程池进行管理
        FutureTask<TaskBean> futureTask = new FutureTask<>(new MyCallableTask());
        Thread thread = new Thread(futureTask);
        thread.start();

        //主线程继续别的逻辑
        System.out.println("main 线程开始执行主流程内耗时操作。。。");

        Thread.sleep(3000);

        //获取子线程的执行结果
        System.out.println("主流程内需要用到子线程的执行结果，这里尝试用阻塞的方式从子线程内获取执行的结果");
        TaskBean taskBean = futureTask.get();

        System.out.println(String.format("获取到的结果是：%s", taskBean.getTaskResult()));
    }


    static class MyCallableTask implements Callable<TaskBean> {

        @Override
        public TaskBean call() throws Exception {

            System.out.println("开始执行子线程任务。。。");

            Thread.sleep(5000);

            System.out.println("子线程任务执行完毕。。。");

            return new TaskBean(1, 10);
        }
    }
}
