package com.wbcoder.callable.multi;

import com.wbcoder.model.TaskBean;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @description: CompletionService多线程并发任务结果归集
 * @author: chengwb
 * @Date: 2020/5/24 15:20
 */
public class CompletionServiceClient {

    public static void main(String[] args) throws Exception {

        //这里搞出来一个线程池

        ExecutorService executor = Executors.newFixedThreadPool(10);
        try {
            //结果集

            List<Integer> res = new ArrayList<>();

            //1.定义CompletionService

            CompletionService<TaskBean> completionService = new ExecutorCompletionService<TaskBean>(executor);

            //2.定义任务列表
            List<Future<TaskBean>> futureList = new ArrayList<>();

            //3、添加任务
            for (int i = 0; i < 10; i++) {
                futureList.add(completionService.submit(new MyTask(i + 1, i * 1000)));
            }

            //子任务都去执行了，这里就去等着把子任务的结果都拿出来进行统一的计算
            //方法1：对结果进行获取
           /* for (Future<TaskBean> future : futureList) {
                TaskBean taskBean = future.get();
                System.out.println(String.format("结果：任务编号：%s ，计算结果 %s ",
                        taskBean.getTaskIndex(),
                        taskBean.getTaskResult()));
                res.add(taskBean.getTaskResult());
            }*/

           //方法2：使用内部阻塞队列的take()
            for (int i = 0; i < 10; i++) {
                TaskBean taskBean = completionService.take().get();
                System.out.println(String.format("结果：任务编号：%s ，计算结果 %s ",
                        taskBean.getTaskIndex(),
                        taskBean.getTaskResult()));
                res.add(taskBean.getTaskResult());
            }

            System.out.println("汇总完毕，下面开始对结果进行友好的展示。。。");

            System.out.println(String.format("res : %s", res.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }


    static class MyTask implements Callable<TaskBean> {

        private Integer taskIndex;
        private Integer sleepTime;

        public MyTask(Integer taskIndex, Integer sleepTime) {
            this.taskIndex = taskIndex;
            this.sleepTime = sleepTime;
        }

        @Override
        public TaskBean call() throws Exception {

            System.out.println(String.format("开始执行任务，任务编号： %s", taskIndex));

            Thread.sleep(sleepTime);

            System.out.println(String.format("任务编号：%s 的任务执行完毕 执行时间 %s，开始上报执行结果", taskIndex, sleepTime));

            if (taskIndex % 3 == 0) {
//                throw new Exception("这里抛出来一个自定义异常，主线程需要怎么处理呢？");
            }

            return new TaskBean(taskIndex, taskIndex * 10);
        }
    }
}
