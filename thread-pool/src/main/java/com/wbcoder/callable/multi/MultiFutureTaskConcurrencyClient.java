package com.wbcoder.callable.multi;

import com.wbcoder.model.TaskBean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * @description: 多任务并发处理
 * FutureTask实现多线程并发执行任务并取结果归集
 * @author: chengwb
 * @Date: 2020/5/24 15:00
 */
public class MultiFutureTaskConcurrencyClient {

    public static void main(String[] args) {

        //准备好一个线程池
        ExecutorService executor = Executors.newFixedThreadPool(10);

        //定义结果集
        List<Integer> res = new ArrayList<>();

        //定义任务列表
        List<FutureTask<TaskBean>> futureTaskList = new ArrayList<>();

        try {

            //把任务批量提交到线程池内
            for (int i = 0; i < 10; i++) {

                //定义好要处理的任务
                FutureTask futureTask = new FutureTask<>(new MyTask(i + 1, i * 1000));

                //把任务丢到线程池内
                executor.submit(futureTask);

                //把任务列表记录下来，方便后期对结果的进行查找
                futureTaskList.add(futureTask);
            }

            System.out.println("开始对子任务的结果进行查看，对结果进行统计。。。");

            //这里是多个任务，要查看每个任务的结果，这里需要进行遍历
            while (futureTaskList.size() > 0) {
                Iterator<FutureTask<TaskBean>> iterator = futureTaskList.iterator();


                while (iterator.hasNext()) {

                    FutureTask<TaskBean> futureTask = iterator.next();


                    //这里直接get阻塞就行
                    if (futureTask.isDone() && !futureTask.isCancelled()) {

                        //阻塞等待任务的执行
                        TaskBean taskBean = futureTask.get();

                        System.out.println(String.format("结果：任务编号 ： %s ，结果 %s ", taskBean.getTaskIndex(), taskBean.getTaskResult()));

                        //对结果进行汇总
                        res.add(taskBean.getTaskResult());

                        //移除掉已经处理过的任务，响应循环的退出条件
                        iterator.remove();
                    } else {
                        //避免CPU高速运转，可以休息一下
                        //延迟效果不好
                        Thread.sleep(1);
                    }
                }
            }

            System.out.println("等待子任务的执行结果的过程已经执行完毕，下面开始对结果进行友好的展示。。。");

            System.out.println(String.format("res : %s ", res.toString()));


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }


    static class MyTask implements Callable<TaskBean> {

        private Integer taskIndex;
        private Integer sleepTime;

        public MyTask(Integer taskIndex, Integer sleepTime) {
            this.taskIndex = taskIndex;
            this.sleepTime = sleepTime;
        }

        @Override
        public TaskBean call() throws Exception {

            System.out.println(String.format("开始执行任务，任务编号： %s", taskIndex));

            Thread.sleep(sleepTime);

            System.out.println(String.format("任务编号：%s 的任务执行完毕 执行时间 %s，开始上报执行结果", taskIndex, sleepTime));

            if (taskIndex % 3 == 0) {
//                throw new Exception("这里抛出来一个自定义异常，主线程需要怎么处理呢？");
            }

            return new TaskBean(taskIndex, taskIndex * 10);
        }
    }
}
