package com.wbcoder.callable.multi;

import com.wbcoder.model.TaskBean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;

/**
 * @description: 多个异步任务提交到线程池，主线程等待多个子线程任务的执行结果,等待的过程利用了future.get的阻塞
 * @author: chengwb
 * @Date: 2020/5/24 13:48
 */
public class MultiTaskConcurrencyClient {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        //准备好一个线程池
        ExecutorService executor = Executors.newFixedThreadPool(10);
        //首先数据分组之后，这里搞一个任务列表
        //结果集
        List<Integer> res = new ArrayList<>();
        List<Future<TaskBean>> futureList = new ArrayList<>();

        try {
            //循环提交任务，到线程池内
            for (int i = 0; i < 10; i++) {
                futureList.add(executor.submit(new MyTask(i + 1, i * 1000)));
            }

            //若干个子任务开始进行结果的执行了，这个时候需要的是主线程等待子线程的执行结果，然后对结果进行汇总
            //这里的问题是：
            //1、如何判断子线程的结果真的执行完了？
            //2、如果其中一个任务发生了异常，怎么对其他的任务进行处理

            //这里解决第一个问题，这么多任务的结果需要判断，这里我就一个一个进行判断
            System.out.println("子任务已经派发完毕，这里开始等待若干个子任务执行完毕....");

            while (futureList.size() > 0) {
                Iterator<Future<TaskBean>> iterator = futureList.iterator();
                //遍历任务列表一遍
                while (iterator.hasNext()) {
                    Future<TaskBean> future = iterator.next();
                    //如果任务完成取结果，否则判断下一个任务是否完成。
                    if (future.isDone() && !future.isCancelled()) {
                        //获取结果
                        //如果这里边有了异常，这里希望跳过，这里就需要进行处理
                        TaskBean taskBean = null;
                        //这种策略是把异常的结果直接忽略掉，然后为了不影响这个的流程，就把这一有遗产的任务直接丢掉，继续执行下面的线程进行执行
//                        try {
//                            taskBean = future.get();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            System.out.println("其中有一个任务发生了异常，这里跳过不处理，不要影响其他线程的执行。");
//                            iterator.remove();
//                            continue;
//                        }

                        taskBean = future.get();
                        System.out.println(String.format("任务编号: %s 执行完毕，取出结果 =%s，开始进行汇总。", taskBean.getTaskIndex(), taskBean.getTaskResult()));

                        //把结果放到内存的一个地方，准备用于后期的汇总

                        res.add(taskBean.getTaskResult());

                        //任务完成移除任务
                        iterator.remove();
                    } else {
                        ////避免CPU高速运转，这里休息1毫秒，CPU纳秒级别
                        Thread.sleep(1);
                    }
                }
            }

            System.out.println("子任务的等待结果执行完毕，这里开始对结果进行统计...");

            System.out.println(String.format("任务结果为：res = %s", res.toString()));
        } catch (Exception e) {
            e.printStackTrace();
            //这里需要对所有的任务进行撤销操作，需要一个遍历
            Iterator<Future<TaskBean>> iterator = futureList.iterator();
            while (iterator.hasNext()) {
                Future<TaskBean> future = iterator.next();
                //这里对这些任务进行一个一个的响应终止
                future.cancel(true);
            }
        } finally {
            executor.shutdown();
        }
    }

    static class MyTask implements Callable<TaskBean> {

        private Integer taskIndex;
        private Integer sleepTime;

        public MyTask(Integer taskIndex, Integer sleepTime) {
            this.taskIndex = taskIndex;
            this.sleepTime = sleepTime;
        }

        @Override
        public TaskBean call() throws Exception {

            System.out.println(String.format("开始执行任务，任务编号： %s", taskIndex));

            Thread.sleep(sleepTime);

            System.out.println(String.format("任务编号：%s 的任务执行完毕 执行时间 %s，开始上报执行结果", taskIndex, sleepTime));

            if (taskIndex % 3 == 0) {
                throw new Exception("这里抛出来一个自定义异常，主线程需要怎么处理呢？");
            }

            return new TaskBean(taskIndex, taskIndex * 10);
        }
    }
}
