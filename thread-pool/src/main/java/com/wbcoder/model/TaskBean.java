package com.wbcoder.model;

public class TaskBean {

    private Integer taskIndex;
    private Integer taskResult;

    public TaskBean(Integer taskIndex, Integer taskResult) {
        this.taskIndex = taskIndex;
        this.taskResult = taskResult;
    }

    public Integer getTaskIndex() {
        return taskIndex;
    }

    public void setTaskIndex(Integer taskIndex) {
        this.taskIndex = taskIndex;
    }

    public Integer getTaskResult() {
        return taskResult;
    }

    public void setTaskResult(Integer taskResult) {
        this.taskResult = taskResult;
    }
}
