package com.wbcoder.pool;

public class MyTask implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(String.format("线程名称：%s , 线程ID：%s , 执行结果：%s",
                Thread.currentThread().getName(),
                Thread.currentThread().getId(),
                "执行完成了。。。"
                )
        );
    }
}
