package com.wbcoder.pool;

import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description: 把这个线程池跑起来
 * @author: chengwb
 * @Date: 2020-04-12 18:57
 */
public class RunThreadPoolClient {

    private static int corePoolSize;
    private static int maximumPoolSize;
    private static long keepAliveTime;
    private static TimeUnit unit;
    private static BlockingQueue blockingQueue;
    private static ThreadFactory threadFactory;
    private static RejectedExecutionHandler rejectedExecutionHandler;
    private static int queueSize;


    static {

        //初始化线程池使用是的一些参数

        corePoolSize = 1;

        maximumPoolSize = 2;

        keepAliveTime = 300;

        unit = TimeUnit.SECONDS;

        queueSize = 1;

        blockingQueue = new ArrayBlockingQueue(queueSize);

        //可以使用默认的，这里自己实现一个
//        threadFactory = Executors.defaultThreadFactory();
        threadFactory = new MyThreadFactory("我自己定义了一个线程组");

        //可以使用默认的，这里自己实现一个
        rejectedExecutionHandler = new ThreadPoolExecutor.AbortPolicy();
//        rejectedExecutionHandler = new MyRejectHandler();
    }

    /**
     * Main方法就是一个 JVM进程
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                    corePoolSize,
                    maximumPoolSize,
                    keepAliveTime,
                    unit,
                    blockingQueue,
                    threadFactory,
                    rejectedExecutionHandler
            );

            threadPoolExecutor.execute(new MyTask());


            /**
             * 这里需要模拟一个触发线程池拒绝策略的情况，把线程池的任务执行速度调慢，队列长度，核心线程池，最大线程池调小，然后多提交几个任务进去测试
             *
             *
             *
             * task rejected. java.util.concurrent.ThreadPoolExecutor@5e2de80c[Running, pool size = 2, active threads = 2, queued tasks = 1, completed tasks = 0]
             * main 线程执行完就返回了。。。
             * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-1 , 线程ID：11 , 执行结果：执行完成了。。。
             * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-2 , 线程ID：12 , 执行结果：执行完成了。。。
             * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-1 , 线程ID：11 , 执行结果：执行完成了。。。
             *
             */
            threadPoolExecutor.execute(new MyTask());
            threadPoolExecutor.execute(new MyTask());
            threadPoolExecutor.execute(new MyTask());
        } catch (Exception e) {
            //这里对线程池的执行过程进行catch处理，如果发生了异常，可以采取策略，不影响这个方法逻辑的继续执行
            e.printStackTrace();
        }


        System.out.println("main 线程执行完就返回了。。。");
    }


    /**
     * 这里对线程池内的创建出来的线程进行一些规则的约束，比如有一些名称的标识
     */
    static class MyThreadFactory implements ThreadFactory {
        /**
         * 这里对线程池内的线程进行一些名称的标识，这里设置两个变量进行命名
         * <p>
         * 前缀+一个自增的ID编号  没特殊的意义
         */
        private final String namePrefix;
        private final AtomicInteger nextId = new AtomicInteger(1);

        public MyThreadFactory(String whatFeatureOfGroup) {
            namePrefix = "MyThreadFactory's " + whatFeatureOfGroup + "-Worker-";
        }

        @Override
        public Thread newThread(Runnable r) {
            String name = namePrefix + nextId.getAndIncrement();
            Thread thread = new Thread(r);
            thread.setName(name);
            return thread;
        }
    }

    /**
     * 线程池拒绝策略的实现
     * <p>
     * JDK的默认实现：
     * <p>
     * 默认调度策略：
     * AbortPolicy:丢弃任务并且抛出 异常。 外层程序需要catch住异常，保证外层程序的继续执行
     * java.util.concurrent.RejectedExecutionException: Task com.wbcoder.pool.MyTask@5e2de80c rejected from java.util.concurrent.ThreadPoolExecutor@1d44bcfa[Running, pool size = 2, active threads = 2, queued tasks = 1, completed tasks = 0]
     * at java.util.concurrent.ThreadPoolExecutor$AbortPolicy.rejectedExecution(ThreadPoolExecutor.java:2063)
     * at java.util.concurrent.ThreadPoolExecutor.reject(ThreadPoolExecutor.java:830)
     * at java.util.concurrent.ThreadPoolExecutor.execute(ThreadPoolExecutor.java:1379)
     * at com.wbcoder.pool.RunThreadPoolClient.main(RunThreadPoolClient.java:81)
     * main 线程执行完就返回了。。。
     * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-1 , 线程ID：11 , 执行结果：执行完成了。。。
     * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-2 , 线程ID：12 , 执行结果：执行完成了。。。
     * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-1 , 线程ID：11 , 执行结果：执行完成了。。。
     * <p>
     * <p>
     * CallerRunsPolicy：调用任务的run方法，绕过线程池的调度，直接运行，会卡住调用线程
     * =======》 main线程卡了 任务的执行时间后才继续往下跑。。 跟异步有冲突
     * 线程名称：main , 线程ID：1 , 执行结果：执行完成了。。。
     * main 线程执行完就返回了。。。
     * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-2 , 线程ID：12 , 执行结果：执行完成了。。。
     * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-1 , 线程ID：11 , 执行结果：执行完成了。。。
     * 线程名称：MyThreadFactory's 我自己定义了一个线程组-Worker-2 , 线程ID：12 , 执行结果：执行完成了。。。
     * <p>
     * DiscardOldestPolicy:抛弃队列内等待最久的任务，然后把当前的任务加入到队列里边。但是丢失却没有事件通知
     * <p>
     * DiscardPolicy:偷偷的丢弃掉，也没有报错信息的提示
     */
    static class MyRejectHandler implements RejectedExecutionHandler {

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            System.out.println("task rejected. " + executor.toString());
        }
    }


    /**
     * 一些开源项目的实现
     * <p>
     * DUBBO
     * <p>
     * 可以看到，当dubbo的工作线程触发了线程拒绝后，主要做了三个事情，原则就是尽量让使用者清楚触发线程拒绝策略的真实原因。
     * <p>
     * 1）输出了一条警告级别的日志，日志内容为线程池的详细设置参数，以及线程池当前的状态，还有当前拒绝任务的一些详细信息。可以说，这条日志，使用dubbo的有过生产运维经验的或多或少是见过的，这个日志简直就是日志打印的典范，其他的日志打印的典范还有spring。得益于这么详细的日志，可以很容易定位到问题所在
     * <p>
     * 2）输出当前线程堆栈详情，这个太有用了，当你通过上面的日志信息还不能定位问题时，案发现场的dump线程上下文信息就是你发现问题的救命稻草。
     * 3）继续抛出拒绝执行异常，使本次任务失败，这个继承了JDK默认拒绝策略的特性
     */
    static class DubboThreadPoolRejectedHandlerAbortPolicyWithReport extends ThreadPoolExecutor.AbortPolicy {

        private final String threadName;

        private final URL url;

        public DubboThreadPoolRejectedHandlerAbortPolicyWithReport(String threadName, URL url) {
            this.threadName = threadName;
            this.url = url;
        }

        private static volatile long lastPrintTime = 0;

        private static Semaphore guard = new Semaphore(1);

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            String msg = String.format("Thread pool is EXHAUSTED!" +
                            " Thread Name: %s, Pool Size: %d (active: %d, core: %d, max: %d, largest: %d), Task: %d (completed: %d)," +
                            " Executor status:(isShutdown:%s, isTerminated:%s, isTerminating:%s), in %s://%s:%d!", threadName, e.getPoolSize(), e.getActiveCount(), e.getCorePoolSize(), e.getMaximumPoolSize(), e.getLargestPoolSize(),
                    e.getTaskCount(), e.getCompletedTaskCount(), e.isShutdown(), e.isTerminated(), e.isTerminating(),
                    url.getProtocol(), url.getHost(), url.getPort());

            System.out.println(msg);
//            logger.warn(msg);
            dumpJStack();
            throw new RejectedExecutionException(msg);
        }

        private void dumpJStack() {
            //省略实现
        }

    }


    /**
     * Netty中的实现很像JDK中的CallerRunsPolicy，舍不得丢弃任务。
     * 不同的是，CallerRunsPolicy是直接在调用者线程执行的任务。
     * 而 Netty是新建了一个线程来处理的。
     * 所以，Netty的实现相较于调用者执行策略的使用面就可以扩展到支持高效率高性能的场景了。
     * 但是也要注意一点，Netty的实现里，在创建线程时未做任何的判断约束，也就是说只要系统还有资源就会创建新的线程来处理，直到new不出新的线程了，才会抛创建线程失败的异常
     */
    static class NettyThreadPoolHandlerNewThreadRunsPolicy implements RejectedExecutionHandler {

        public NettyThreadPoolHandlerNewThreadRunsPolicy() {
            super();
        }

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            try {
                final Thread t = new Thread(r, "Temporary task executor");
                t.start();
            } catch (Throwable e) {
                throw new RejectedExecutionException(
                        "Failed to start a new thread", e);
            }
        }
    }


    /**
     * activeMq中的策略属于最大努力执行任务型，
     * 当触发拒绝策略时，在尝试一分钟的时间重新将任务塞进任务队列，
     * 当一分钟超时还没成功时，就抛出异常
     */
    static class ActiveMqThreadPoolHandler implements RejectedExecutionHandler {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            try {
                executor.getQueue().offer(r, 60, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                throw new RejectedExecutionException("Interrupted waiting for BrokerService.worker");
            }

            throw new RejectedExecutionException("Timed Out while attempting to enqueue Task.");
        }
    }

    /**
     * pinpoint的拒绝策略实现很有特点，和其他的实现都不同。
     * 他定义了一个拒绝策略链，包装了一个拒绝策略列表，
     * 当触发拒绝策略时，会将策略链中的rejectedExecution依次执行一遍。
     */
    static class pinpointThreadPoolHandlerRejectedExecutionHandlerChain implements RejectedExecutionHandler {

        private final RejectedExecutionHandler[] handlerChain;

        public static RejectedExecutionHandler build(List<RejectedExecutionHandler> chain) {
            Objects.requireNonNull(chain, "handlerChain must not be null");
            RejectedExecutionHandler[] handlerChain = chain.toArray(new RejectedExecutionHandler[0]);
            return new pinpointThreadPoolHandlerRejectedExecutionHandlerChain(handlerChain);
        }

        public pinpointThreadPoolHandlerRejectedExecutionHandlerChain(RejectedExecutionHandler[] handlerChain) {
            this.handlerChain = Objects.requireNonNull(handlerChain, "handlerChain must not be null");
        }

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            for (RejectedExecutionHandler rejectedExecutionHandler : handlerChain) {
                rejectedExecutionHandler.rejectedExecution(r, executor);
            }
        }
    }
}

