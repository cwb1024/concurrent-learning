package com.wbcoder.pool;

import java.util.concurrent.*;

/**
  * @description: 搞一个线程池，这个线程池的线程都是以守护线程的方式进行运行
 *  线程池内的线程如果以守护线程的方式进行运行，那么如果主线程运行退出后，线程池内所有线程都将不再运行，这个线程运行的代码处可能就只执行以一段
  * @author: chengwb
  * @Date: 2020-04-13 01:43
  */
public class RunTomcatThreadPoolClient {


    /**
     *
     * 程序的运行结果是：
     *
     * main thread run end.
     * 0
     *
     *
     * 并没有打印出线程的finally内的代码
     *
     *
     *
     * @param args
     */

    public static void main(String[] args) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                10, 10, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setName("我自己的池子：");
                thread.setDaemon(true);
                return thread;
            }
        }, new ThreadPoolExecutor.AbortPolicy());

        threadPoolExecutor.execute(new MyTomcatTask());
        System.out.println("main thread run end.");


    }


   static class MyTomcatTask implements Runnable{

        @Override
        public void run() {
            try {
                for (int i = 0; i < 5; i++) {
                    System.out.println(i);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }finally {
                System.out.println("线程异常终止，这个finally内的代码将不会执行。。。");
            }
        }
    }
}
