package com.wbcoder.pool;

public class ThreadPoolClient {

    //把一个int 值，拆成高位低位，当作两个功能来用，有什么好处？
    /**
     *
     * 善用位运算，有时候可以给我们节省很多空间。
     * 但是，在这里，明显不是为了省空间了，因为就算用两个值分开表示状态和工作线程数量，也就8个字节而已。
     *
     *
     * 为了在多线程环境下保证运行状态和线程数量的统一。
     * 把这两个值放到一个 int 变量中，然后用 AtomicInteger 进行存储和读写，就可以保证这两个值始终是统一的。
     * 如果用两个变量保存，即使用了 AtomicInteger ，也可能出现一个改了，另一个还没改的情况。
     *
     */

    //工作线程数：用最右边的29位表示工作线程数，用左边3位表示线程池的状态
    private static final int COUNT_BITS = Integer.SIZE - 3;

    // 000-11111 11111111 11111111 11111111
    private static final int CAPACITY   = (1 << COUNT_BITS) - 1;
    // 111-00000 00000000 00000000 00000000
    // 这里模拟一个取反运算后的位表示
    private static final int _CAPACITY = ~CAPACITY;


    /**
     * 下面的定义可以发在表示运行状态的时候，低位都是0，只取高位的值表示线程池的运行状态，这与设计初衷是相对应的
     */

    //111-000000 00000000 00000000 00000000
    private static final int RUNNING    = -1 << COUNT_BITS;
    //000-000000 00000000 00000000 00000000
    private static final int SHUTDOWN   =  0 << COUNT_BITS;
    //001-000000 00000000 00000000 00000000
    private static final int STOP       =  1 << COUNT_BITS;
    //010-000000 00000000 00000000 00000000
    private static final int TIDYING    =  2 << COUNT_BITS;
    //011-000000 00000000 00000000 00000000
    private static final int TERMINATED =  3 << COUNT_BITS;

    /**
     *
     * & 按位与操作，同时为1，操作后才为1
     *
     * ～ 这个操作是怎么理解的呢？   这是取反运算符,所有位 0与1对调
     *
     * ｜ 或运算，相同位有1，操作后就是1
     *
     * 这个线程的容量的设计是很巧妙的，高位都是0，低位都是1，表示了可以容纳的线程执行的最大数，然后取反运算后，高位都是1，低位都是0表示了当前线程池的状态
     *
     * @param c  这个c 既包含了线程池的运行状态，也包含了线程的个数
     * @return
     */
    private static int runStateOf(int c)     { return c & ~CAPACITY; }

    /**
     * 按位与运算算出正在运行的线程数，这个比较好理解，比较值，最左边都是0，线程个数都是1，---》 跟当前的这个数量进行比较，就可以得到当前运行的数量了
     * @param c
     * @return
     */
    private static int workerCountOf(int c)  { return c & CAPACITY; }

    /**
     *
     * 把左边的3位和右边的29位进行按位或运算，合成一个数？
     *
     * 首先 左边3位的取值就是 运行状态的值呗，左边有几个可能出现1，右边都是0
     *      右边29位的取值就是，线程运行的个数呗，左边都是0，右边29位可能出现1，
     *
     *      进行或操作后，这个值就既保存了线程池的运行状态，也保留了工作线程的个数
     *
     *      对这个值进行高位低位的与操作，就可以拿到想要的状态了
     *
     * @param rs
     * @param wc
     * @return
     */
    private static int ctlOf(int rs, int wc) { return rs | wc; }

    private static boolean isRunning(int c) {
        return c < SHUTDOWN;
    }

    /**
     * 那个什么时候这个线程池的运行状态会进行变更呢，我猜是程序内在使用的时候会进行判断，然后进行状态的变更，也有可能让认为的干预线程的执行，有API提供出去
     * @param args
     */

    public static void main(String[] args) {

        /**
         *
         * 计算后的数值
         *
         * 29          29个低位保存工作线程数
         * 536870911   5亿
         *
         *
         * -536870912   运行状态，表示还可以继续接受新的任务
         * 0            此状态表示：不再接受新的任务，但是还可以继续执行队列内的任务
         * 536870912    此状态表示：全面拒绝，并且中断正在处理的任务
         * 1073741824   此状态表示：所有的任务已经被终止
         * 1610612736   此状态表示：已经清理完现场
         *
         *
         */
        System.out.println(COUNT_BITS);
        System.out.println(CAPACITY);
        System.out.println(RUNNING);
        System.out.println(SHUTDOWN);
        System.out.println(STOP);
        System.out.println(TIDYING);
        System.out.println(TERMINATED);
    }
}
