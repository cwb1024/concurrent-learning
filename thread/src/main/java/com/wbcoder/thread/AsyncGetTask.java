package com.wbcoder.thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class AsyncGetTask implements Runnable {

    private FutureTask futureTask;

    public AsyncGetTask(FutureTask futureTask) {
        this.futureTask = futureTask;
    }

    @Override
    public void run() {
        try {
            //这个自线程开始在这里获取一个异步处理的结果
            System.out.println("AsyncGetTask start get ... " + System.currentTimeMillis());
            System.out.println("AsyncGetTask end get ... " + System.currentTimeMillis() + "  ,and result is " + futureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
