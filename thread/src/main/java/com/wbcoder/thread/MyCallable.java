package com.wbcoder.thread;

import java.util.concurrent.Callable;

public class MyCallable implements Callable {
    @Override
    public Object call() throws Exception {
        for (int i = 0; i < 100; i++) {
            Thread.sleep(100);
            System.out.println(i);
        }
        return "hello";
    }
}
