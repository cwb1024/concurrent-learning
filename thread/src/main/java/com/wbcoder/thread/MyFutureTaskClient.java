package com.wbcoder.thread;

import java.util.concurrent.FutureTask;
import java.util.concurrent.locks.LockSupport;

public class MyFutureTaskClient {

    public static void main(String[] args) throws InterruptedException {


        FutureTask futureTask = new FutureTask<>(new MyCallable());

        Thread tFutureTask = new Thread(futureTask);

        tFutureTask.start();



        //由于会阻塞主线程，这里搞一个自线程，开始进行结果的统计

//        try {
//            Object o = futureTask.get();
//            System.out.println(o);
//        } catch (Exception e) {
//
//        }

        Thread tResult = new Thread(new AsyncGetTask(futureTask));

        tResult.start();

//        while (true){
//            Thread.sleep(200);
//            System.out.println("Main Thread running....");
//        }

    }
}
