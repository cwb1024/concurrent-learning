package com.wbcoder.thread.order;

public class MainThreadJoin {

    public static void main(String[] args) throws Exception{

        final Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第一个线程需要执行的指令...");
            }
        });


        final Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第二个线程需要执行的指令...");
            }
        });

        final Thread thread03 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第三个线程需要执行的指令...");
            }
        });


        thread01.start();
        thread01.join();


        thread02.start();
        thread02.join();

        thread03.start();
        thread03.join();


        /**
         *
         * 主线程控制唤醒每一个自线程的调用，一个一个调用，按照顺序执行
         *
         *
         *
         */

    }
}
