package com.wbcoder.thread.order;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleThreadPool {

    static ExecutorService pool = Executors.newSingleThreadExecutor();

    public static void main(String[] args) {

        final Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第一个线程需要执行的指令...");
            }
        });


        final Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第二个线程需要执行的指令...");
            }
        });

        final Thread thread03 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第三个线程需要执行的指令...");
            }
        });


        pool.submit(thread01);

        pool.submit(thread02);

        pool.submit(thread03);

        pool.shutdown();
    }
}
