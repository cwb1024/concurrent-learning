package com.wbcoder.thread.order;

/**
 * @description: 多个线程需要通过程序强制约束他的顺序执行
 * @author: chengwb
 * @Date: 2020/10/2 15:42
 */
public class SubThreadJoin {

    public static void main(String[] args) {

        final Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第一个线程需要执行的指令...");
            }
        });


        final Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    thread01.join();


                    System.out.println("这里是第二个线程需要执行的指令...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });


        final Thread thread03 = new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    thread02.join();


                    System.out.println("这里是第三份线程需要执行的指令...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        });

        thread01.start();
        thread02.start();
        thread03.start();

        /**
         *
         * 每个子线程控制着需要等待的别的线程
         *
         *
         */

    }
}
