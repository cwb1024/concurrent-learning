package com.wbcoder.thread.order;

public class SubThreadWait {

    private static Object myLock1 = new Object();
    private static Object myLock2 = new Object();

    private static Boolean t1Run = false;
    private static Boolean t2run = false;

    public static void main(String[] args) {

        final Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (myLock1) {
                    System.out.println("这里是第一个线程需要执行的指令...");
                    t1Run = true;
                    myLock1.notify();
                }
            }
        });


        final Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (myLock1) {
                    try {
                        if (!t1Run) {
                            myLock1.wait();
                        }


                        synchronized (myLock2) {
                            System.out.println("这里是第二个线程需要执行的指令...");
                            myLock2.notify();
                        }
                    } catch (Exception e) {

                    }
                }
            }
        });


        final Thread thread03 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (myLock2) {
                    try {

                        if (!t2run) {
                            myLock2.wait();
                        }

                        System.out.println("这里是第三个线程需要执行的指令...");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread01.start();

        thread02.start();

        thread03.start();
    }
}
