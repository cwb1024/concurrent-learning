package com.wbcoder.thread.order;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadCondition {

    private static Lock lock = new ReentrantLock();
    private static Condition condition01 = lock.newCondition();
    private static Condition condition02 = lock.newCondition();

    private static Boolean t1Run = false;
    private static Boolean t2Run = false;


    public static void main(String[] args) {


        final Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();

                System.out.println("这里是第一个线程需要执行的指令...");

                t1Run = true;

                condition01.signal();

                lock.unlock();
            }
        });


        final Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();

                try {

                    if (!t1Run) {
                        condition01.await();
                    }

                    System.out.println("这里是第二个线程需要执行的指令...");

                    t2Run = true;

                    condition02.signal();

                } catch (Exception e) {

                }
                lock.unlock();
            }
        });


        final Thread thread03 = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();

                try {
                    if (!t2Run) {
                        condition02.await();
                    }


                    System.out.println("这里是第三个线程需要执行的指令...");


                } catch (Exception e) {


                }

                lock.unlock();
            }
        });


        thread01.start();
        thread02.start();
        thread03.start();
    }

}
