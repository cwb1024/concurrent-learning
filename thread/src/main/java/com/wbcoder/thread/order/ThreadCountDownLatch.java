package com.wbcoder.thread.order;

import java.util.concurrent.CountDownLatch;

public class ThreadCountDownLatch {

    /**
     * 用于判断第一个线程是否执行，执行之后进行减一操作
     */
    private static CountDownLatch c1 = new CountDownLatch(1);

    /**
     * 用于判断第二个线程是否执行，执行之后进行减一操作
     */
    private static CountDownLatch c2 = new CountDownLatch(1);

    public static void main(String[] args) {

        final Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第一个线程需要执行的指令...");
                c1.countDown();
            }
        });


        final Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    c1.await();

                    System.out.println("这里是第二个线程需要执行的指令...");

                    c2.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        final Thread thread03 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    c2.await();


                    System.out.println("这里是第三个线程需要执行的指令...");


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread01.start();

        thread02.start();

        thread03.start();

    }
}
