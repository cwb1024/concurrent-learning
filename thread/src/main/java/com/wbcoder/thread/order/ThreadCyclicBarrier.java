package com.wbcoder.thread.order;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ThreadCyclicBarrier {

    static CyclicBarrier barrier01 = new CyclicBarrier(2);

    static CyclicBarrier barrier02 = new CyclicBarrier(2);

    public static void main(String[] args) {


        final Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("这里是第一个线程需要执行的指令...");

                    //放开栅栏01
                    barrier01.await();

                } catch (Exception e) {

                }
            }
        });

        final Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //放开栅栏01
                    barrier01.await();

                    System.out.println("这里是第二个线程需要执行的指令...");

                    //放开栅栏02
                    barrier02.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }
        });

        final Thread thread03 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //放开栅栏02
                    barrier02.await();

                    System.out.println("这里是第三个线程需要执行的指令...");

                } catch (Exception e) {

                }
            }
        });


        thread01.start();

        thread02.start();

        thread03.start();
    }
}
