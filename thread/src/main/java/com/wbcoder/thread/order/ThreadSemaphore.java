package com.wbcoder.thread.order;

import java.util.concurrent.Semaphore;

public class ThreadSemaphore {

    private static Semaphore semaphore01 = new Semaphore(1);
    private static Semaphore semaphore02 = new Semaphore(1);


    public static void main(String[] args) {

        final Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是第一个线程需要执行的指令...");
                semaphore01.release();
            }
        });


        final Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    semaphore01.acquire();
                    System.out.println("这里是第二个线程需要执行的指令...");
                    semaphore02.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        final Thread thread03 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    semaphore02.acquire();
                    thread02.join();
                    semaphore02.release();
                    System.out.println("这里是第三个线程需要执行的指令...");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread01.start();
        thread02.start();
        thread03.start();
    }

}
